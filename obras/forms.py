# -*- encoding: utf-8 -*-
from django import forms
from obras import models
from ajax_select import make_ajax_field, get_lookup
from obras.models import DetalleGasto, Honorario, PagoObra

import types
from ajax_select.fields import AutoCompleteSelectWidget, bootstrap
from django.utils.safestring import mark_safe
from django.utils.html import escapejs
from django.core.urlresolvers import reverse
from django.forms.util import flatatt
from django.template.loader import render_to_string

#ADMIN FORMS
class UniqueModelForm(forms.ModelForm):
    """ 
    Model form that provides a method to control case insensitive uniqueness
    of fields.
    """
    
    def _clean_unique(self, field_name, error_string):
        field_value = self.cleaned_data[field_name]
        
        if field_value:
            args = {field_name + '__iexact' : field_value}
            models = self.Meta.model.objects.filter(**args)
            
            if self.instance.id:
                models = models.exclude(id=self.instance.id)
            
            if models:
                raise forms.ValidationError(error_string)
        
        return field_value
    

class PaisAdminForm(UniqueModelForm):
    
    def clean_descripcion(self):
        return self._clean_unique('descripcion', 
                """ Ya existe un país con esa descripción. """)
    
    class Meta:
        model = models.Pais


class ProveedorAdminForm(UniqueModelForm):
    
    def clean_nombre(self):
        return self._clean_unique('nombre', 
                """ Ya existe un proveedor con ese nombre. """)
    
    class Meta:
        model = models.Proveedor
        
class ClienteAdminForm(UniqueModelForm):
    
    def clean_nombre(self):
        return self._clean_unique('nombre', 
                """ Ya existe un cliente con ese nombre. """)
    
    class Meta:
        model = models.Cliente


class PagoObraAdminForm(forms.ModelForm):
    
    proveedor = forms.ModelChoiceField(label="Proveedor", 
                                       queryset=models.Proveedor.objects)
    
    obra = forms.ModelChoiceField(label="Obra",
                                queryset=models.Obra.objects)
    
    
    
    nro_presup_externo = forms.CharField(label="Número presupuesto externo",
                                         required=False, max_length=100, 
                        widget=forms.TextInput(attrs={'readonly': 'readonly', 
                                                      'style':'border:0;'}))
    estado_de_obra = forms.CharField(required=False, max_length=100, 
                        widget=forms.TextInput(attrs={'readonly': 'readonly', 
                                                      'style':'border:0;'}))
    descripcion_pres = forms.CharField(label="Descripción presupuesto",
                        required=False, max_length=100, 
                        widget=forms.TextInput(
                        attrs={'readonly': 'readonly', 'style':'border:0;'}))
    observaciones_pres = forms.CharField(label="Observaciones presupuesto",
                        required=False, max_length=10000, 
                        widget=forms.TextInput(                                               
                            attrs={'readonly': 'readonly', 'style':'border:0;'}))
    importe_contratado = forms.CharField(required=False, max_length=100, 
                        widget=forms.TextInput(attrs={'readonly': 'readonly', 
                                                      'style':'border:0;'}))
    total_aplicaciones = forms.CharField(required=False, max_length=100, 
                        widget=forms.TextInput(attrs={'readonly': 'readonly', 
                                                      'style':'border:0;'}))
    saldo = forms.CharField(required=False, max_length=100, 
                        widget=forms.TextInput(attrs={'readonly': 'readonly', 
                                                      'style':'border:0;'}))
    
    descripcion = forms.CharField(label="Descripción", max_length=130, 
                        widget=forms.TextInput(                                               
                            attrs={'style':"width:610px;"}))

    def clean(self):
        super(PagoObraAdminForm, self).clean()
        
        # Validacion de numero de factura repetido
        cleaned_data = self.cleaned_data
        n_factura = cleaned_data.get('n_factura')
        tipo_factura = cleaned_data.get('tipo_factura')
        
        pagoobra_iguales = PagoObra.objects.filter(n_factura=n_factura, tipo_factura=tipo_factura)
        
        if n_factura and tipo_factura and pagoobra_iguales:
            if len(pagoobra_iguales) == 1 and pagoobra_iguales[0].id == self.instance.id:
                # Se esta modificando el mismo objeto
                pass
            else:
                msg = 'El número de factura ya se encuentra en el sistema'
                self._errors['n_factura'] = self.error_class([msg])
            
                del cleaned_data['n_factura']

        
        # Validacion de presupuesto
        presupuesto = self.cleaned_data.get('presupuesto')
        if presupuesto:
            saldo = (presupuesto.importe_contratado - 
                            presupuesto.total_aplicaciones())
            
            if self.instance.imp_neto:
                saldo += self.instance.imp_neto
            
            imp_neto = self.cleaned_data.get('imp_neto')
            if imp_neto and saldo < imp_neto:
                raise forms.ValidationError(
                        'El importe neto supera el saldo del presupuesto.')
        
        return self.cleaned_data

    class Meta:
        model = models.PagoObra


class HonorarioAdminForm(forms.ModelForm):
    
    descripcion_pres = forms.CharField(label='Descripcion presupuesto', 
                        required=False, max_length=100, 
                        widget=forms.TextInput(attrs={'readonly': 'readonly', 
                                                      'style':'border:0;'}))
    estado_de_obra = forms.CharField(required=False, max_length=100, 
                        widget=forms.TextInput(attrs={'readonly': 'readonly', 
                                                      'style':'border:0;'}))
    importe_contratado = forms.CharField(required=False, max_length=100, 
                        widget=forms.TextInput(attrs={'readonly': 'readonly', 
                                                      'style':'border:0;'}))
    total_aplicaciones = forms.CharField(required=False, max_length=100, 
                        widget=forms.TextInput(attrs={'readonly': 'readonly', 
                                                      'style':'border:0;'}))
    importe_proyectado = forms.CharField(required=False, max_length=100, 
                        widget=forms.TextInput(attrs={'readonly': 'readonly', 
                                                      'style':'border:0;'}))
    saldo = forms.CharField(required=False, max_length=100, 
                        widget=forms.TextInput(attrs={'readonly': 'readonly', 
                                                      'style':'border:0;'}))
    
    def clean(self):
        cleaned_data = self.cleaned_data
        n_factura = cleaned_data.get('n_factura')
        tipo_factura = cleaned_data.get('tipo_factura')
        
        honorarios_iguales = Honorario.objects.filter(n_factura=n_factura, tipo_factura=tipo_factura)
        
        if n_factura and tipo_factura and honorarios_iguales:
            if len(honorarios_iguales) == 1 and honorarios_iguales[0].id == self.instance.id:
                # Se esta modificando el mismo objeto
                pass
            else:
                msg = 'El número de factura ya se encuentra en el sistema'
                self._errors['n_factura'] = self.error_class([msg])
            
                del cleaned_data['n_factura']
        
        return cleaned_data 
            
    
    def clean_obra(self):
        
        presupuestos = models.PresupuestoTSYA.objects.filter(
                                            obra=self.cleaned_data['obra'])
        
        if not presupuestos:
            raise forms.ValidationError(
                    'La obra seleccionada no tiene un Presupuesto TSYA.')
                
        return self.cleaned_data['obra']

    class Meta:
        model = models.Honorario
        

class PresupuestoAdminForm(forms.ModelForm):
    
    cantidad_de_presupuestos = forms.CharField(required=False, max_length=100,
                        widget=forms.TextInput(attrs={'readonly': 'readonly', 
                                                      'style':'border:0;'}))
    
    id = forms.CharField(label='ID', required=False, max_length=100,
                        widget=forms.TextInput(attrs={'readonly': 'readonly', 
                                                      'style':'border:0;'}))
    
    total_presupuesto_asignado = forms.CharField(required=False, max_length=100,
                        widget=forms.TextInput(attrs={'readonly': 'readonly', 
                                                      'style':'border:0;'}))
    
    total_de_aplicaciones = forms.CharField(required=False, max_length=100,
                        widget=forms.TextInput(attrs={'readonly': 'readonly', 
                                                      'style':'border:0;'}))
    
    saldo_presupuesto_disponible = forms.CharField(required=False, max_length=100,
                        widget=forms.TextInput(attrs={'readonly': 'readonly', 
                                                      'style':'border:0;'}))
    

    class Meta:
        model = models.Presupuesto

class PresupuestoTSYAAdminForm(forms.ModelForm):
    
    cliente = forms.ModelChoiceField(label="Cliente", 
                                queryset=models.Cliente.objects)
    
    estado_obra = forms.CharField(label="Estado de obra", 
                        required=False, max_length=100,
                        widget=forms.TextInput(attrs={'readonly': 'readonly', 
                                                      'style':'border:0;'}))
    class Meta:
        model = models.PresupuestoTSYA

class GastoAdminForm(forms.ModelForm):
    
    detalles = forms.CharField(label="Detalle breve", 
                              widget=forms.TextInput(attrs={'size': 85}))
    
    total_aplicado = forms.CharField(label="Total aplicado", required=False, max_length=30,
                        widget=forms.TextInput(attrs={'readonly': 'readonly', 
                                                      'style':'border:0;'}))
    
    falta_aplicar = forms.CharField(label='Falta aplicar', required=False, max_length=30,
                        widget=forms.TextInput(attrs={'readonly': 'readonly', 
                                                      'style':'border:0;'}))
    
    
    def clean(self):
        
        # Validacion de numero de factura repetido x proveedor
        cleaned_data = self.cleaned_data
        n_factura = cleaned_data.get('numero')
        tipo_factura = cleaned_data.get('tipo')
        proveedor = cleaned_data.get('proveedor')
        
        gasto_iguales = models.Gasto.objects.filter(numero=n_factura,
                                                    tipo=tipo_factura, proveedor=proveedor)
     
        if n_factura and tipo_factura and proveedor and gasto_iguales:
            if len(gasto_iguales) == 1 and gasto_iguales[0].id == self.instance.id:
                # Se esta modificando el mismo objeto
                pass
            else:
                msg = 'El número/tipo de factura ya se encuentra para este proveedor'
                self._errors['numero'] = self.error_class([msg])
            
                del cleaned_data['numero']
        
        return cleaned_data
    
    class Meta:
        model = models.Gasto
        


def fixed_clean(self, value):
    """ 
    Version modificada del clean del AutocompleteSelectField, que fallaba
    cuando el value era 0.
    """
    if value is not None:
        lookup = get_lookup(self.channel)
        objs = lookup.get_objects( [ value] )
        if len(objs) != 1:
            # someone else might have deleted it while you were editing
            # or your channel is faulty
            # out of the scope of this field to do anything more than tell you it doesn't exist
            raise forms.ValidationError(u"%s cannot find object: %s" % (lookup,value))
        return objs[0]
    else:
        if self.required:
            raise forms.ValidationError(self.error_messages['required'])
        return None

class FixedAutoCompleteWidget(AutoCompleteSelectWidget):
    def render(self, name, value, attrs=None):

        if not value and value != 0:
            value = ''
        
        final_attrs = self.build_attrs(attrs)
        self.html_id = final_attrs.pop('id', name)

        lookup = get_lookup(self.channel)
        if value or value == 0:
            objs = lookup.get_objects([value])
            try:
                obj = objs[0]
            except IndexError:
                raise Exception("%s cannot find object:%s" % (lookup, value))
            display = lookup.format_item_display(obj)
            current_repr = mark_safe( """new Array("%s",%s)""" % (escapejs(display),obj.pk) )
        else:
            current_repr = 'null'

        context = {
                'name': name,
                'html_id' : self.html_id,
                'min_length': getattr(lookup, 'min_length', 1),
                'lookup_url': reverse('ajax_lookup',kwargs={'channel':self.channel}),
                'current_id': value,
                'current_repr': current_repr,
                'help_text': self.help_text,
                'extra_attrs': mark_safe(flatatt(final_attrs)),
                'func_slug': self.html_id.replace("-",""),
                'add_link' : self.add_link,
                }
        context.update(bootstrap())
        
        return mark_safe(render_to_string(('autocompleteselect_%s.html' % self.channel, 'autocompleteselect.html'),context))

def do_make_ajax_field(model, model_fieldname, channel, **kwargs):
    """ Crea el field ajax y lo parchea cambiando el metodo clean. """
    
    widget = FixedAutoCompleteWidget(channel)
    field = make_ajax_field(model, model_fieldname, channel, widget=widget, **kwargs)
    field.clean = types.MethodType(fixed_clean, field, field.__class__)
    return field

class DetalleGastoAdminForm(forms.ModelForm):
    
    detalle = forms.CharField(required=False, 
                            label="Detalle breve", widget=forms.TextInput(attrs={'size': 30}))
    
    categoria = forms.ModelChoiceField(label="Categoría", queryset=models.Categoria.objects.all())
    
    obra = do_make_ajax_field(DetalleGasto, 'obra', 'obra', help_text=None)
    
    def __init__(self, *args, **kwargs):
        super(DetalleGastoAdminForm, self).__init__(*args, **kwargs)
        
        # agrego la categoria inicial al form que se calcula a partir del concepto
        if 'instance' in kwargs:
            detalle_gasto = kwargs['instance']
            self.fields['categoria'].initial = detalle_gasto.concepto.categoria

    class Meta:
        model = models.DetalleGasto

class ObraAdminForm(UniqueModelForm):
    
    def clean_nombre(self):
        return self._clean_unique('nombre', 
                """ Ya existe una obra con ese nombre. """)
    
    class Meta:
        model = models.Obra
        
class EstadoObraAdminForm(UniqueModelForm):
    
    def clean_descripcion(self):
        return self._clean_unique('descripcion', 
                """ Ya existe un estado de obra con ese nombre. """)
    
    class Meta:
        model = models.EstadoObra
        
class ProvinciaAdminForm(forms.ModelForm):
    
    def clean(self):
        super(ProvinciaAdminForm, self).clean()
        
        descripcion = self.cleaned_data.get('descripcion')
        pais = self.cleaned_data.get('pais')
        
        if descripcion and pais:
            pcias = models.Provincia.objects.filter(descripcion__iexact=descripcion, 
                                                    pais=pais)
            
            if self.instance.id:
                pcias = pcias.exclude(id=self.instance.id)
            
            if pcias:
                msg = u" Ya existe una provincia con esa descripcion. "
                self._errors['descripcion'] = self.error_class([msg])
                del self.cleaned_data['descripcion']
                
        
        return self.cleaned_data
    
    class Meta:
        model = models.Provincia