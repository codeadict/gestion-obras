from django import template
from obras import models

from datetime import date

register = template.Library()

@register.inclusion_tag('obras/fechas.html')
def dif_fechas(object_id):
    try:
        fecha = models.Obra.objects.get(id__exact=object_id).fecha_creacion
    
    except:
        return { 'fecha': None, 'fecha_actual': None }
    
    fecha_actual = date.today()
    return { 'fecha': fecha, 'fecha_actual': fecha_actual }


@register.inclusion_tag('reports/gastos_x_obra.html', takes_context=True)
def gastos_x_obra(context, obra):
    """
    Gastos x obra. render inclusion tag
    """
    
    #Takes vars from context
    categoria = context['categoria']
    desde = context['desde']
    hasta = context['hasta']
    proveedor = context['proveedor']
    concepto = context['concepto']

    if categoria:
        categorias = [categoria]
    else:
        categorias = obra.get_categorias(desde, hasta)

    items= []
    for cat in categorias:
        print cat
        super_parcial = []

        superconceptos = cat.get_superconceptos()
        if concepto and concepto in superconceptos:
            superconceptos = [concepto]

        for superconcepto in superconceptos:
            print superconcepto
            sub_parcial = []
            subconceptos = superconcepto.get_subconceptos()
                            
            if concepto and not concepto.is_superconcepto():
                if concepto in subconceptos:
                    subconceptos = [concepto]
                else:
                    subconceptos = []

            for subconcepto in subconceptos:
                print 'sub >>> %s' % subconcepto
                gastos = models.DetalleGasto.objects.filter(
                    obra=obra,
                    concepto=subconcepto,
                    gasto__fecha_op__range=(desde, hasta)
                )
                #filtrar x proveedor
                if proveedor:
                    gastos = gastos.filter(gasto__proveedor=proveedor)

                print [gasto.imp_neto for gasto in gastos]

                total = sum([gasto.imp_neto for gasto in gastos])
                if total:
                    sub_parcial.append((subconcepto, gastos, total))
            
            total = sum([parcial[2] for parcial in sub_parcial])
            if total:
                super_parcial.append((superconcepto, sub_parcial, total))
        total = sum([parcial[2] for parcial in super_parcial])
        if total:
            items.append((cat, super_parcial, total))
            
    #total General
    #total = sum([i[2] for i in items])
    #print items

    return {'items': items}

def percentage(value, arg):
    """ Devuelve el porcentage de arg que representa value. """
    
    try :
        value = float(str(value))
        arg = float(str(arg))
        
        percentage = value * 100 / arg
        
        return ("%.2f%%" % (percentage)).replace('.', ',') 
        
    except:
        return ''
    
register.filter('percentage', percentage)
