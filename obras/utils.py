from datetime import date
from django.db.models.fields import FieldDoesNotExist
from django.utils.numberformat import format

def comma(d):
    ''' devuelve el digito con puntos y comas
    Ej: d=11111.12 --> 11.111,12 '''
    s = '%0.2f' % d
    a,b = s.split('.')
    l = []
    while len(a) > 3:
        l.insert(0,a[-3:])
        a = a[0:-3]
    if a:
        l.insert(0,a)
    return '.'.join(l)+','+b

def first_of_month():
        today = date.today()
        return today.replace(today.year, today.month, 1)
    

def name_pretty_print(name):
    return name.capitalize().replace('_', ' ')

def field_pretty_print(field_name, model):
    
    if '__' in field_name:
        #Handle foreing key field
        
        field_names = field_name.split('__')
        field_name = field_names[-1]
        for i in range(len(field_names) - 1):
            model = model._meta.get_field(field_names[i]).related.parent_model
            
    if hasattr(model, field_name) and callable(getattr(model, field_name)):
        #Handle function
        function = getattr(model, field_name)
        if hasattr(function,'short_description'):
            return function.short_description
        else:
            return name_pretty_print(field_name)
        
    try:
        return model._meta.get_field(field_name).verbose_name.capitalize()
    except FieldDoesNotExist:
        return name_pretty_print(field_name)
    
def money_format(value):
    return '$' + format(value, ',', 2, 3, '.')