# -*- encoding: utf-8 -*-
from django.contrib import admin
from obras.models import *

from obras import forms
from django.contrib import auth
from obras.forms import DetalleGastoAdminForm
from django.forms.models import BaseInlineFormSet
from django.core.exceptions import ValidationError


class ContactoProveedorInline(admin.StackedInline):
    model = ContactoProveedor
    extra = 1
    
    fieldsets = (
        (None, {
            'fields': (('nombre', 'email', 'telefono'), 
                       ('celular', 'nextel','pin'))
        }),
    )

class ProveedorAdmin(admin.ModelAdmin):
    
    form = forms.ProveedorAdminForm
    
    inlines = [ContactoProveedorInline,]
    
    fieldsets = (
        (None, {
            'fields': (('id', 'nombre',), ('domicilio', 'localidad'), 
                       ('cod_postal', 'cod_prov',), 
                       ('telefono', 'telefono_alternativo', 'fax',),
                       ('cuit', 'cod_impuesto'),('email', 'web'))
        }),
    )
    list_display = ('id', 'nombre',)
    list_display_links = ('nombre', )
    readonly_fields = ('id',)
    search_fields = ['nombre', 'id']
    readonly_fields = ('id',)
    ordering = ('-id',)

class ContactoClienteInline(admin.StackedInline):
    model = ContactoCliente
    extra = 1
    
    fieldsets = (
        (None, {
            'fields': (('nombre', 'email', 'telefono'), 
                       ('celular', 'nextel','pin'))
        }),
    )

class ClienteAdmin(admin.ModelAdmin):
    inlines = [ContactoClienteInline,]
    
    form = forms.ClienteAdminForm
    
    fieldsets = (
        (None, {
            'fields': (('id', 'nombre'), ('domicilio', 'localidad',),
                       ('cod_postal', 'cod_prov',), 
                       ('telefono', 'telefono_alternativo', 'fax',), 
                       ('cuit', 'cod_impuesto',), ('email', 'web'), 'observaciones')
        }),
    )
    list_display = ('id', 'nombre')
    list_display_links = ('nombre',) 
    readonly_fields = ('id',)
    search_fields = ['nombre', 'id']
    readonly_fields = ('id',)
    ordering = ('-id',)

class DetalleGastoFormset(BaseInlineFormSet):
    
    def clean(self):
        
        imp_neto = Decimal(0)
        for form in self.forms:
            try:
                if form.cleaned_data:
                    imp_neto += form.cleaned_data['imp_neto']
            except AttributeError:
                # annoyingly, if a subform is invalid Django explicity raises
                # an AttributeError for cleaned_data
                pass
        
        # Si no es un numero va a tirar error el form de arriba
        try:
            saldo = Decimal(self.data.get('imp_neto'))
        except:
            return
        
        if saldo < imp_neto:
            raise ValidationError('El importe neto supera el saldo del gasto.')
        
            


class DetalleGastoInline(admin.TabularInline):
    
    model = DetalleGasto
    form = forms.DetalleGastoAdminForm
    readonly_fields = ('importe_a_pagar',) 
    extra = 0
    formset = DetalleGastoFormset
    

        
    def id_concepto(self, detalle):
        try: 
            return unicode(detalle.concepto.pk)
        except:
            return u''
    
    def importe_a_pagar(self, detalle):
        return detalle.get_importe_a_pagar()
    
    fieldsets = (
        (None, {
            'fields': ('categoria','concepto', 'obra', 'detalle',
             'imp_neto','importe_a_pagar',) 
        }),
    )
    
class DetalleGastoAdmin(admin.ModelAdmin):
    
    model = DetalleGasto
    form = DetalleGastoAdminForm
    fields = ('gasto','categoria','concepto','obra','detalle','imp_neto','importe_a_pagar') 
    readonly_fields = ('importe_a_pagar',)

    def id_concepto(self, detalle):
        try: 
            return unicode(detalle.concepto.pk)
        except:
            return u''
    
    def importe_a_pagar(self, detalle):
        return detalle.get_importe_a_pagar()
   
       

class GastoAdmin(admin.ModelAdmin):
    form = forms.GastoAdminForm
    inlines = [DetalleGastoInline,]
   
    readonly_fields = ('id',)
    
    fieldsets = (
        (None, {
            'fields': (('id', 'fecha',), ('numero', 'fecha_op', 'tipo',), 'proveedor', 
                       'detalles', 'observaciones', 'moneda', 'imp_neto')
        }),
        
        ('Aplicaciones', {
            'classes' : ('aplicaciones',),
            'fields': ('total_aplicado', 'falta_aplicar')
        }),
        
    )
    
    list_display = ('id', 'proveedor', 'detalles')
    list_display_links = ('detalles', )
    ordering = ('-id',)
    search_fields = ('id', 'detalles', 'proveedor__nombre')
    
#TODO issue 47002
#    def save_model(self, request, obj, form, change):
#        obj.save()
#        gasto = obj
#        
#    def armar_presupuesto(gasto):
#        presupuesto = Presupuesto()
#        presupuesto.
        
        
    
class ObraAdmin(admin.ModelAdmin):
    form = forms.ObraAdminForm
    
    fieldsets = (
        (None, {
            'fields': ('id', 'nombre', 'fecha_creacion', 'cliente', 'pais',
                       'direccion', 'estado_obra', 'observaciones')
        }),
    )
    
    list_display = ('id', 'nombre', 'cliente', 'estado_obra',)
    list_display_links = ('nombre', )
    readonly_fields = ('id',)
    search_fields = ['nombre', 'estado_obra__descripcion', 'id']
    ordering = ('-id',)

class EstadoObraAdmin(admin.ModelAdmin):
    
    form = forms.EstadoObraAdminForm
    
    fieldsets = (
        (None, {
            'fields': ('id', 'descripcion')
        }),
    )
    readonly_fields = ('id',)
    list_display = ('id', 'descripcion',)
    list_display_links = ('descripcion', )
    ordering = ('-id',)

class PresupuestoAdmin(admin.ModelAdmin):
    form = forms.PresupuestoAdminForm

    fieldsets = (
                 
        (None, {
            'fields': (('id', 'fecha_emision',), ('obra', 'num_pres_ext', 'detalle_pres_ext'),
             ('proveedor', 'cantidad_de_presupuestos',), 'descripcion', 
             'observaciones', ('moneda', 'importe_contratado',))
        }),
    
        ("Saldo", {
            'fields': ('total_presupuesto_asignado', 'total_de_aplicaciones', 
                       'saldo_presupuesto_disponible')
        }),
    )
    
    
    list_display = ('id', 'descripcion', 'proveedor', 'obra')
    list_display_links = ('descripcion', )
    
    search_fields = ('id', 'descripcion', 'proveedor__nombre', 'obra__nombre', )
    ordering = ('-id',)

class PresupuestoTSYAAdmin(admin.ModelAdmin):
    form = forms.PresupuestoTSYAAdminForm
    
    def total_presupuesto_asignado(self, presupuesto):
        return presupuesto.importe_contratado or ''
    
    def total_de_aplicaciones(self, presupuesto):
        try:
            return presupuesto.obra.total_aplicaciones()
        except:
            return ''
    
    def saldo_presupuesto_disponible(self, presupuesto):
        try:
            return (self.total_presupuesto_asignado(presupuesto) -
                    self.total_de_aplicaciones(presupuesto))
        except:
            return ''

    fieldsets = (
        (None, {
            'fields': (('id', 'fecha_emision',), ('cliente', 'obra', 'estado_obra'), 
                       'descripcion', 'observaciones', 
                       ('moneda', 'importe_contratado'),
                       'importe_proyectado')
        }),
        
        ("Saldo", {
            'fields': ('total_presupuesto_asignado', 'total_de_aplicaciones', 
                       'saldo_presupuesto_disponible')
        }),
    )
    readonly_fields = ('id','total_presupuesto_asignado', 'total_de_aplicaciones', 
                       'saldo_presupuesto_disponible')
    
    list_display = ('id', 'descripcion', 'obra')
    list_display_links = ('descripcion', )
    ordering = ('-obra__id',)
    search_fields = ('id', 'descripcion', 'obra__nombre', 'obra__id')

class HonorarioAdmin(admin.ModelAdmin):
    form = forms.HonorarioAdminForm
    
    fieldsets = (
        (None, {
            'fields': (('tipo_factura', 'n_factura', 'fecha',), 'descripcion', 'observaciones')
        }),
        ('Aplicacion', {
             'fields': ('obra', 'descripcion_pres', 'estado_de_obra',
             ('importe_contratado', 'importe_proyectado'), 
             ('total_aplicaciones','saldo'), ('imp_neto', 'imp_a_cobrar'),)
        }),
    )
    
    list_display = ('n_factura', 'descripcion', 'obra')
    search_fields = ('n_factura', 'descripcion', 'obra__nombre')
    list_display_links = ('descripcion', )
    ordering = ('-n_factura',)

class PagoObraAdmin(admin.ModelAdmin):
    form = forms.PagoObraAdminForm
    
    def importe_a_cobrar(self, pago):
        return unicode(pago.get_importe_a_cobrar())
    
    def proveedor(self, pago):
        return pago.presupuesto.proveedor.nombre
    
    def obra(self, pago):
        return pago.presupuesto.obra.nombre

    fieldsets = (
        (None, {
            'fields': (( 'id'), ('tipo_factura', 'n_factura', 'fecha_emision'),
                    'descripcion', 'observaciones')
        }),
        ('Aplicacion', {
             'fields': (('obra', 'proveedor'), ('presupuesto', 'nro_presup_externo',),
             ('estado_de_obra'),
             'descripcion_pres', 'observaciones_pres', 'importe_contratado',
             ('total_aplicaciones', 'saldo'), ('imp_neto', 'imp_a_cobrar'))
        }),
    )
    readonly_fields = ('id',)
    list_display = ('id', 'descripcion', 'proveedor', 'obra')
    list_display_links = ('descripcion', )
    ordering = ('-id',)
    search_fields = ('id', 'descripcion', 'presupuesto__proveedor__nombre', 
                     'presupuesto__obra__nombre', )

class TipoComprobanteAdmin(admin.ModelAdmin):
    list_display = ('descripcion','codigo',)
    exclude = ('activo',)



class ConceptoAdmin(admin.ModelAdmin):
    exclude = ('activo',)
    list_display = ('codigo_categoria', 'codigo', 'descripcion',)
    list_display_links = ('descripcion', )
    search_fields = ['descripcion', 'codigo', 'categoria__codigo']
    
    def codigo_categoria(self, concepto):
        return concepto.categoria.codigo
    codigo_categoria.short_description = 'Código categoría'
    
class CategoriaAdmin(admin.ModelAdmin):
    exclude = ('activo',)
    list_display = ('codigo', 'descripcion',)
    list_display_links = ('descripcion', )
    ordering = ('codigo',)
    
class PaisAdmin(admin.ModelAdmin):
    
    form = forms.PaisAdminForm
    
    exclude = ('activo',)
    list_display = ('id', 'descripcion',)
    list_display_links = ('descripcion', )
    ordering = ('-id',)
    
class ProvinciaAdmin(admin.ModelAdmin):
    form = forms.ProvinciaAdminForm
    
    exclude = ('activo',)
    list_display = ('id', 'descripcion',)
    list_display_links = ('descripcion', )
    ordering = ('-id',)


class UserAdmin(auth.admin.UserAdmin):
    
    fieldsets = (
        (None, {
            'fields': ('username', 'password', 'first_name', 'last_name', 'email', )
        }),
        ('Permisos', {
             'fields': ('is_staff', 'is_superuser', 'groups', 'user_permissions',)
        }),
    )

    
        
admin.site.register(Proveedor, ProveedorAdmin)
admin.site.register(Gasto, GastoAdmin)
admin.site.register(Obra, ObraAdmin)
admin.site.register(EstadoObra, EstadoObraAdmin)
admin.site.register(Cliente, ClienteAdmin)
admin.site.register(Presupuesto, PresupuestoAdmin)
admin.site.register(PresupuestoTSYA, PresupuestoTSYAAdmin)
admin.site.register(Honorario, HonorarioAdmin)
admin.site.register(Pais, PaisAdmin)
admin.site.register(Provincia, ProvinciaAdmin)
admin.site.register(TipoComprobante, TipoComprobanteAdmin)
admin.site.register(Concepto, ConceptoAdmin)
admin.site.register(Categoria, CategoriaAdmin)
admin.site.register(DetalleGasto, DetalleGastoAdmin)
admin.site.register(PagoObra, PagoObraAdmin)
admin.site.unregister(auth.models.User)
admin.site.register(auth.models.User, UserAdmin)
admin.site.register(auth.models.Permission)


