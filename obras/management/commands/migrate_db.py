# -*- encoding: utf-8 -*-
'''
Creates a valid default super user.
Runs with python manage.py superuser
'''
from django.core.management.base import BaseCommand
import settings
import csv
import datetime
from obras import models
from django.utils.datastructures import SortedDict
from django.db.utils import DatabaseError, IntegrityError
from decimal import Decimal

FILE_PATH = settings.PROJECT_DIR + '/csv/'

DESCRIPCION_PAIS = {1 : 'Argentina',
                    2 : 'Brasil',
                    3 : 'Panamá',
                    4 : 'Uruguay',
                    5 : 'Paraguay',
                    6 : 'Chile',
                    7 : 'Centro America',
                    8 : 'Costa Rica',
                    9 : 'Republica Dominicana',
                    10 : 'Venezuela',
                    11 : 'Peru',
                    12 : 'Colombia',
                    13 : 'España',                    
                    }
DEFAULT_PAS = "País"
DESCRIPCION_PROVINCIA = {1 : 'Capital Federal',
                         2 : 'Buenos Aires',
                         3 : 'La pampa',
                         4 : 'Chaco',
                         7 : 'Jujuy',
                         12 : 'Tucuman',
                         13 : 'San Luis',
                         14 : 'San Juan',
                         16 : 'Santiago del Estero',
                         17 : 'Santa Fe',
                         18 : 'Cordoba',
                         20 : 'Corrientes',
                         22 : 'Salta',
                         23 : 'Catamarca',
                         24 : 'Misiones',
                         25 : 'Entre Rios',
                         26 : 'Neuquen',
                         28 : 'Chubut',
                         29 : 'Rio Negro',
                         30 : 'Santa Cruz',
                         31 : 'La Rioja',
                         32 : 'Mendoza',
                         33 : 'Formosa',}
DEFAULT_PROVINCIA = "Provincia"
ESTADOS = {1 : 'En gestión',
           4 : 'En curso', 
           3 : 'Terminado',
           5 : 'Desactivado', 
           6 : 'Archivado',           
           }
CODIGO_ESTADO = {   541 : 1, 551 : 1, 5071 : 1, 5981 : 1,
                    542 : 4, 552 : 4, 5072 : 4, 5982 : 4,
                    543 : 3, 553 : 3, 5073 : 3, 5983 : 3,
                    544 : 5, 554 : 5, 5074 : 5, 5984 : 5,
                    545 : 6, 555 : 6, 5075 : 6, 5985 : 6,}
PAIS_ESTADO = {   541 : 1, 551 : 2, 5071 : 3, 5981 : 4,
                  542 : 1, 552 : 2, 5072 : 3, 5982 : 4,
                  543 : 1, 553 : 2, 5073 : 3, 5983 : 4,
                  544 : 1, 554 : 2, 5074 : 3, 5984 : 4,
                  545 : 1, 555 : 2, 5075 : 3, 5985 : 4,}
DEFAULT_ESTADO = "Estado obra"



def limpiar_observaciones(obs):
    try:
        obs = unicode(obs, errors='ignore')
        if '{' in obs:
            return ''
        return obs
    except UnicodeDecodeError:
        return ''
    

def limpiar_id(id_str):
    id_str = id_str.split('.')[0]
    return int(id_str)
    

def limpiar_telefono(telefono):
    #TODO inlcuir todos los telefonos cuando sean strings
    #return int(telefono.replace(' ', '').replace('-', '').split('/')[0])
    return telefono[:50]

def limpiar_fecha(fecha):
    if not fecha:
        return None
    
    try:
        return datetime.datetime.strptime(fecha, "%Y-%m-%d").date()
    except ValueError:
        return datetime.datetime.strptime(fecha, "%y-%m-%d").date()

def limpiar_importe(importe):
    return Decimal(importe or '0.00')

def limpiar_entero(numero):
    numero = numero.split('.')[0]
    
    return int(numero) if numero else 0

def limpiar_booleano(bool):
    return bool == '1'

def resolver_pais(cod_pais):
    
    return models.Pais.objects.get(id=int(cod_pais))

def pais_por_estado(cod_estado):
    return resolver_pais(PAIS_ESTADO[int(cod_estado)])

def resolver_provincia(cod_prov):
    if not cod_prov:
        cod_prov = '1'
    
    descripcion = DESCRIPCION_PROVINCIA.get(int(cod_prov), 'Provincia') 
    
    provincia = models.Provincia.objects.get_or_create(id=int(cod_prov),
                                    defaults={'descripcion' : descripcion,
                                              'pais' : resolver_pais("1")})[0]
    
    return provincia

def resolver_tipo_comprobante(cod_tipo):
    
    tipo, created = models.TipoComprobante.objects.get_or_create(
                                                        codigo=cod_tipo)
    
    if created:
        tipo.descripcion = cod_tipo
        tipo.save()
    
    return tipo

def resolver_categoria(codigo):
    
    tipo, created = models.Categoria.objects.get_or_create(
                                                        codigo=codigo)
    
    if created:
        tipo.descripcion = "CATEGORIA"
        tipo.save()
    
    return tipo

def crear_estados():
    for estado in ESTADOS:
        estado_model = models.EstadoObra()
        estado_model.id = estado
        estado_model.descripcion = ESTADOS[estado]
        estado_model.save()
        

def resolver_estado(codigo):
    
    codigo_nuevo = CODIGO_ESTADO.get(int(codigo), 1)
    return models.EstadoObra.objects.get(id=codigo_nuevo)

""" 
Las funciones handler leen un archivo csv y vuelcan sus datos a modelos. 
"""
#TODO el patron de cada handler es identico, refactorizar en una clase

def handle_pais(doc):
    print "Migrando Pais"
    
    #sacar los excludes
    models.Pais.objects.all().delete()
    
    doc.next()
    
    for row in doc:
        
        pais = models.Pais()
        
        pais.id = limpiar_id(row[0])
        pais.descripcion = row[1] 
        pais.activo = limpiar_booleano(row[2]) 
        
        pais.save()

def handle_provincia(doc):
    print "Migrando Provincias"
    
    models.Provincia.objects.all().delete()
    
    doc.next()
    
    for row in doc:
        
        provincia = models.Provincia()
        
        provincia.id = limpiar_id(row[0])
        provincia.descripcion = row[1] 
        provincia.activo = limpiar_booleano(row[2]) 
        provincia.pais = resolver_pais(row[3])
        
        provincia.save()
    
    print "Fin migracion provincia."
    
def handle_tipo_comprobante(doc):
    print "Migrando Tipo Comprobante"
    
    models.TipoComprobante.objects.all().delete()
    
    doc.next()
    
    for row in doc:
        
        tipo = models.TipoComprobante()
        tipo.codigo = row[0]
        tipo.descripcion = row[1]
        
        tipo.save()
    
    print "Fin migracion Tipo comprobante."

def handle_proveedor(doc):
    
    print "Migrando Proovedor"
    
    models.Proveedor.objects.all().delete()
    
    doc.next() #skip header
    
    for row in doc:
        
        proveedor = models.Proveedor()
        proveedor.id = limpiar_id(row[0])
        proveedor.nombre = row[1]
        proveedor.domicilio = row[2]
        proveedor.localidad = row[3]
        
        proveedor.cod_prov = resolver_provincia(row[4])
        
        proveedor.cod_postal = row[5]
        proveedor.telefono = limpiar_telefono(row[6])
        proveedor.fax = limpiar_telefono(row[7])
        proveedor.cuit = limpiar_entero(row[8])        
        proveedor.cod_impuesto = limpiar_entero(row[9])
        
        proveedor.save()
    
    print "Fin migracion proveedor."
        
def handle_cliente(doc):
    
    print "Migrando Cliente"
    
    models.Cliente.objects.all().delete()
    
    doc.next() #skip header
    
    for row in doc:
        
        cliente = models.Cliente()
        cliente.id = limpiar_id(row[0])
        cliente.nombre = row[1]
        cliente.domicilio = row[2]
        cliente.localidad = row[3]
        cliente.cod_prov = resolver_provincia(row[4])
        
        cliente.cod_postal = row[5]
        cliente.telefono = limpiar_telefono(row[6])
        cliente.fax = limpiar_telefono(row[7])
        cliente.cuit = limpiar_entero(row[8])
        cliente.cod_impuesto = limpiar_entero(row[9])
        
        cliente.observaciones = limpiar_observaciones(row[11])
        
        cliente.activo = limpiar_booleano(row[10])
        
        cliente.save()
        
    print "Fin migracion cliente."

def crear_obra_cero():
    obra = models.Obra()
    obra.id = 0
    obra.nombre = "NO IMPUTABLES A OBRA"
    obra.cliente = models.Cliente.objects.get(id=17)
    obra.pais = resolver_pais(1)
    obra.estado_obra = resolver_estado("1")
    
    obra.save()

def handle_obra(doc):
    
    print "Migrando Obra"
    
    models.Obra.objects.all().delete()
    
    crear_estados()
    crear_obra_cero()
    
    doc.next() #skip header
    
    for row in doc:
        obra = models.Obra()
        obra.id = limpiar_id(row[0])
        
        obra.activo = limpiar_booleano(row[5])
        obra.estado_obra = resolver_estado(row[4])
        
        #Solo obra no desactivadas
        if obra.estado_obra.id != 5:
         
            obra.nombre = row[1]
            obra.fecha_creacion = limpiar_fecha(row[2])
            obra.cliente = models.Cliente.objects.get(id=int(row[3]))
                
            obra.pais = pais_por_estado(row[4])
            obra.observaciones = limpiar_observaciones(row[6])
                
            obra.save()
    
    print "Fin migracion obra."
        
    
    

def handle_presupuesto(doc):
    
    print "Migrando Presupuesto"
    
    models.PresupuestoTSYA.objects.all().delete()
    
    doc.next() #skip header
    
    for row in doc:
        
        presupuesto = models.PresupuestoTSYA()
        presupuesto.id = limpiar_id(row[0])
        presupuesto.fecha_emision = limpiar_fecha(row[7])
        
        try:
            presupuesto.obra = models.Obra.objects.get(id=limpiar_id(row[1]))
        except models.Obra.DoesNotExist:
            print "No existe obra", limpiar_id(row[1])
            continue
        
        presupuesto.descripcion = limpiar_observaciones(row[5]) or 'HONORARIOS'
        presupuesto.observaciones = limpiar_observaciones(row[6])
        presupuesto.moneda = limpiar_entero(row[2])
        
        #TODO guardar importe proyectado en campo oculto
        presupuesto.importe_contratado = limpiar_importe(row[3])
        presupuesto.importe_proyectado = limpiar_importe(row[4])
        
        presupuesto.save()
        
    print "Fin migracion presupuesto."

def handle_orden_de_pago(doc):
    
    print "Migrando Orden de pago"
    
    models.Gasto.objects.all().delete()
    
    doc.next() #skip header
    
    for row in doc:
        gasto = models.Gasto()
        gasto.id = limpiar_id(row[0])
        gasto.fecha = limpiar_fecha(row[9])  
        gasto.fecha_op = limpiar_fecha(row[1]) or gasto.fecha 
        gasto.numero = limpiar_entero(row[3])
        gasto.tipo = resolver_tipo_comprobante(row[2])
        
        try:
            gasto.proveedor = models.Proveedor.objects.get(id=limpiar_id(row[4]))
        except:
            print "Proveedor no existe", row[4]
            continue
        
        gasto.detalles = limpiar_observaciones(row[7]) or 'Sin detalles'
        gasto.observaciones = limpiar_observaciones(row[8])
        gasto.moneda = limpiar_entero(row[5])
        gasto.imp_neto = limpiar_importe(row[6] or '0.00')
        
        #fulero
        if gasto.numero > 2147483647:
            continue
                            
        try:
            gasto.save()
        except DatabaseError:
            print gasto.moneda, gasto.imp_neto, gasto.numero
    
    print "Fin migracion orden de pago."
        
        
def handle_comprobante_obra(doc):
    print "Migrando Comprobante obra"
    
    models.Presupuesto.objects.all().delete()
    
    doc.next() #skip header
    
    for row in doc:
        presupuesto = models.Presupuesto()
        presupuesto.codigo_comprobante = limpiar_id(row[0])
        presupuesto.fecha_emision = limpiar_fecha(row[9])
        
        try:
            presupuesto.obra = models.Obra.objects.get(id=limpiar_id(row[1]))
        except models.Obra.DoesNotExist:
            print "No existe obra", limpiar_id(row[1])
            continue
        
        presupuesto.num_pres_ext = row[2]
        
        try:
            presupuesto.proveedor = models.Proveedor.objects.get(id=limpiar_id(row[3]))
        except:
            print "Provedor no existe", row[4]
            continue
        
        presupuesto.moneda = limpiar_entero(row[5])
        presupuesto.importe_contratado = limpiar_importe(row[6])
        presupuesto.descripcion = limpiar_observaciones(row[7]) or 'Comprobante'
        presupuesto.observaciones = limpiar_observaciones(row[8])
        
        presupuesto.save()
    
    print "Fin migracion comprobante obra."

def handle_factura(doc):
    print "Migrando Factura"
    
    models.Honorario.objects.all().delete()
    
    doc.next() #skip header
    
    for row in doc:
        
        honorario = models.Honorario()
        honorario.id = limpiar_id(row[0])
        
        honorario.fecha = limpiar_fecha(row[1])
        honorario.tipo_factura = resolver_tipo_comprobante(row[2])
        honorario.n_factura = limpiar_entero(row[3])
        
        honorario.imp_a_cobrar = limpiar_importe(row[4])
        honorario.imp_neto = limpiar_importe(row[5])
        
        honorario.descripcion = limpiar_observaciones(row[6])
        honorario.observaciones = limpiar_observaciones(row[7]) 
        
        try:
            pres = models.PresupuestoTSYA.objects.get(id=limpiar_id(row[9]))
            honorario.obra = pres.obra
        except models.PresupuestoTSYA.DoesNotExist:
            print "No existe presupuesto", limpiar_id(row[9])
            continue
        
        try:
            honorario.save()
        except IntegrityError:
            print "Honorario clave duplicada", row[3]
        
    
    print "Fin migracion factura."

def handle_factura_obra(doc):
        
    print "Migrando Factura obra"
    
    models.PagoObra.objects.all().delete()
    
    doc.next() #skip header
    
    for row in doc:
        pago = models.PagoObra()
        pago.id = limpiar_id(row[0])
        
        try:
            pago.presupuesto = models.Presupuesto.objects.get(
                                codigo_comprobante=limpiar_id(row[9]), 
                                obra__id=limpiar_id(row[10]))
        except models.Presupuesto.DoesNotExist:
            print "No existe presupuesto", limpiar_id(row[9])
            continue
        
        pago.fecha_emision = limpiar_fecha(row[1]) or pago.presupuesto.fecha_emision
        pago.fecha = limpiar_fecha(row[12])
        
        pago.tipo_factura = resolver_tipo_comprobante(row[2])
        pago.n_factura = limpiar_entero(row[3])
        
        pago.imp_a_cobrar = limpiar_importe(row[4])
        pago.imp_neto = limpiar_importe(row[5])
        
        pago.descripcion = limpiar_observaciones(row[6]) or 'Pago obra'
        pago.observaciones = limpiar_observaciones(row[7])
        
        #fulero
        if pago.n_factura > 2147483647:
            continue
        
        
        pago.save()
    
    print "Fin migracion factura obra."


def clean_concepto(concepto):
    if '.' not in concepto:
        concepto += '.'
    
    concepto += "0000"
    
    return concepto[:8].split('.')
    
    

def handle_detalle_orden(doc):
    print "Migrando Detalle orden de pago"
    
    models.DetalleGasto.objects.all().delete()
    
    doc.next() #skip header
    
    for row in doc:
        detalle = models.DetalleGasto()
        
        if row[1]:
            detalle.id = limpiar_id(row[1])
        
        try:
            detalle.gasto = models.Gasto.objects.get(id=limpiar_id(row[0]))    
        except:
            print "No existe Gasto", row[0], row[1]
            continue
        
        try:
            categoria, codigo = clean_concepto(row[2])
            
            detalle.concepto = models.Concepto.objects.get(codigo=codigo, 
                                                           categoria__codigo=categoria)
        except models.Concepto.DoesNotExist:
            print "No existe Cocepto", limpiar_id(row[2])
            continue
        
        try:
            detalle.obra = models.Obra.objects.get(id=limpiar_id(row[3]))
        except models.Obra.DoesNotExist:
            print "No existe Obra", limpiar_id(row[3])
            continue
        
        detalle.detalle = limpiar_observaciones(row[4])
        detalle.imp_neto = limpiar_importe(row[6])
        
        detalle.save()
    
    print "Fin migracion detalle orden de pago."

def make_no_asignados():
    for categoria in ['100', '200', '300', '400']:
        concepto = models.Concepto()
        concepto.categoria =  resolver_categoria(categoria)
        concepto.codigo = "9999"
        concepto.descripcion = "NO ASIGNADO"
        concepto.no_asignado = True
        concepto.save()

def handle_concepto(doc):
    
    print "Migrando Concepto."
    
    models.Concepto.objects.all().delete()

    make_no_asignados()
    
    doc.next() #skip header
    
    for row in doc:
        concepto = models.Concepto()
        
        #FIXME, guardar las categorias aparte
        codigo_categoria, concepto.codigo = row[0].split('.')
        
        concepto.categoria = resolver_categoria(codigo_categoria)        
        
        concepto.descripcion = row[1]
        concepto.activo = limpiar_booleano(row[2])
        
        concepto.save()
    
    print "Fin migracion concepto."
    
#FIXME, medio feo agregar uno por uno
HANDLERS = SortedDict()
HANDLERS['Pais.csv'] = handle_pais
HANDLERS['Provincia.csv'] = handle_provincia
HANDLERS['Tipo de Comprobante.csv'] = handle_tipo_comprobante
HANDLERS['Proveedor.csv'] = handle_proveedor
HANDLERS['Cliente.csv'] = handle_cliente
HANDLERS['Concepto.csv'] = handle_concepto
HANDLERS['Obra.csv'] = handle_obra
HANDLERS['Presupuesto.csv'] = handle_presupuesto
HANDLERS['Comprobante Obra.csv'] = handle_comprobante_obra
HANDLERS['Factura Obra.csv'] = handle_factura_obra
HANDLERS['Orden de Pago.csv'] = handle_orden_de_pago
HANDLERS['Factura.csv'] = handle_factura
HANDLERS['nueva.csv'] = handle_detalle_orden

class Command(BaseCommand):
    
    def handle(self, *args, **options):
        
        for filename in HANDLERS:
            f = open(FILE_PATH + filename, 'r')
            doc = csv.reader(f)
            HANDLERS[filename](doc)
        
        print "Fin migracion."

