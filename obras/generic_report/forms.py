from django import forms
from django.contrib.admin import widgets
from datetime import date
from obras import utils
from obras.report_forms import BaseReportForm
from obras import models
from obras.generic_report import report_items

class ElementSelectForm(forms.Form):
    ELEMENTO_CHOICES = (('HON', 'Honorario'), 
                        ('DET', 'Detalle de gasto'),
                        ('PAG', 'Pago de obra'),
                        ('PRE', 'Presupuesto proveedor'),
                        ('TSY', 'Presupuesto honorarios TSYA'),
                        )
    elemento = forms.ChoiceField(choices=ELEMENTO_CHOICES)
    

class GenericReportForm(BaseReportForm):
    
    desde = forms.DateField(widget=widgets.AdminDateWidget(), initial=utils.first_of_month)
    hasta = forms.DateField(widget=widgets.AdminDateWidget(), initial=date.today)
    
    agrupar = forms.ChoiceField(label='Agrupar por', choices=[], initial='NO') 
    
    total_general = forms.BooleanField(initial=True, required=False)
    totales_parciales = forms.BooleanField(initial=False, required=False)
    
    def __init__(self, *args, **kwargs):
        super(GenericReportForm, self).__init__(*args, **kwargs)
        self._agregar_campos()
        self._agregar_filtros_querysets()
        self._update_agrupar_choices()
    
    def _model(self):
        return self.Meta.report_class.Meta.model
    
    def _update_agrupar_choices(self):
        choices = [('NO', 'No agrupar')]
        
        index = 0
        for campo in self.Meta.agrupar_por:
            choices.append((campo,  utils.field_pretty_print(campo, 
                                                    self._model())))
            index += 1
        
        self.fields['agrupar'].choices = choices
    
    def _agregar_campos(self):
        for field_name in self.Meta.campos:
            label = utils.field_pretty_print(field_name, self._model())
            form = forms.BooleanField(required=False, initial=True, 
                                      label=label)
                                      
            self.fields['campo_' + field_name] = form
        
    def _agregar_filtros_querysets(self):
        for field_name in self.Meta.agrupar_por:
            
            model_class = self.Meta.agrupar_por[field_name]
            
            form = forms.ModelChoiceField(queryset=model_class.objects, 
                        required=False)
                        
            self.fields[field_name] = form
            
    def get_campos_fields(self):
        return [self['campo_' + f] for f in self.Meta.campos]
    
    def get_grupos_fields(self):
        return [self[f] for f in self.Meta.agrupar_por]
    
    
    def _get_agrupar_choice(self, grupo):
        """ 
        Devuelve el nombre del campo del modelo al que corresponde el
        grupo indicado.
        
        por ejemplo, dado 'cliente' devuelve 'obra__cliente' si esta entre los
        campos o el mismo nombre del grupo.
        """
        
        for campo in self.Meta.campos:
            if campo.endswith(grupo):
                return campo
        
        return grupo
    
    def _get_filtros_dict(self):
        """ 
        Devuelve un diccionario de instancias por las que filtrar los elementos
        del reporte. 
        """
        
        filtros = {}
        
        for field in self.Meta.agrupar_por:
            value = self.cleaned_data.get(field)
            if value:
                filtros[self._get_agrupar_choice(field)] = value
        
        return filtros
    
    def _make_context_dict(self):
        
        desde = self.cleaned_data['desde']
        hasta = self.cleaned_data['hasta']
        parciales = self.cleaned_data['totales_parciales']
        general = self.cleaned_data['total_general']
        
        columnas = [c for c in self.Meta.campos 
                            if self.cleaned_data['campo_' + c]]
        
        filtros = self._get_filtros_dict()
        
        if self.cleaned_data['agrupar'] == 'NO':
            agrupar_por = None
        else:
            agrupar_por = self._get_agrupar_choice(str(
                                                self.cleaned_data['agrupar']))
        
        reporte = self.Meta.report_class(desde, hasta, columnas, 
                               agrupar_por, filtros,
                               parciales, general)
        
        return { 'fecha': date.today(), 'pagesize': 'A4', 
                'columnas' : columnas, 'reporte' : reporte}
    
    
    
    class Meta:
        """ Modelo al que corresponden los items del reporte. """
        report_class = None
        
        """ 
        Lista de los campos a ser incluidos en el reporte. Son redefinidos por 
        las subclases. 
        """
        campos = []
        
        """ 
        Campos por los que se pueden agrupar los items del reporte.
        Debe ser un diccionario con items del estilo field_name:model_class.
        
        Las opciones de agrupado tambien seran las de filtrado.       
        """
        agrupar_por = {}
        

class HonorarioReportForm(GenericReportForm):
    
    class Meta:
        report_class = report_items.HonorarioReport
        campos = ['tipo_factura', 'n_factura', 'fecha', 'obra', 
                  'obra__pais', 'obra__cliente', 'obra__estado_obra',                  
                  'imp_neto', 'imp_a_cobrar']
        agrupar_por = {'obra' : models.Obra, 'pais' : models.Pais, 
                       'cliente' : models.Cliente, 'estado_obra' : models.EstadoObra}

class GastoReportForm(GenericReportForm):
    
    class Meta:
        report_class = report_items.GastoReport
        campos = ['tipo', 'numero', 'fecha_op', 'proveedor', 'imp_neto', 
                  'get_importe_a_pagar', 'total_aplicaciones']
        agrupar_por = {'proveedor': models.Proveedor }

class DetalleReportForm(GenericReportForm):
    
    class Meta:
        report_class = report_items.DetalleReport
        campos = ['concepto', 'concepto__categoria', 
                  'obra', 'obra__pais', 'obra__cliente', 'obra__estado_obra',
                  'gasto__fecha_op', 'gasto__proveedor', 'gasto', 
                  'imp_neto', 'get_importe_a_pagar']
        
        agrupar_por = {'obra' : models.Obra,
                       'pais' : models.Pais, 
                       'cliente' : models.Cliente, 
                       'estado_obra' : models.EstadoObra,
                       'proveedor' : models.Proveedor,
                       'concepto' : models.Concepto,
                       'categoria' : models.Categoria}

class PagoReportForm(GenericReportForm):
    
    class Meta:
        report_class = report_items.PagoReport
        campos = ['tipo_factura', 'n_factura', 'fecha', 'presupuesto', 
                  'presupuesto__proveedor', 'presupuesto__obra',
                  'presupuesto__obra__pais', 'presupuesto__obra__cliente',
                  'presupuesto__obra__estado_obra',
                  'imp_neto', 'imp_a_cobrar']
        agrupar_por = {'obra' : models.Obra, 
                       'pais' : models.Pais, 
                       'cliente' : models.Cliente, 
                       'estado_obra' : models.EstadoObra,
                       'proveedor' : models.Proveedor, 
                       }

class PresupuestoReportForm(GenericReportForm):
    
    class Meta:
        report_class = report_items.PresupuestoReport
        campos = ['num_pres_ext', 'fecha_emision', 'proveedor', 'obra',
                  'importe_contratado', 'total_aplicaciones']
        agrupar_por = {'obra' : models.Obra, 
                       'proveedor' : models.Proveedor}

class PresupuestoTSYAReportForm(GenericReportForm):
    
    class Meta:
        report_class = report_items.PresupuestoTSYAReport
        campos = ['fecha_emision', 'obra', 
                  'obra__pais', 'obra__estado_obra',
                  'importe_contratado',
                  'importe_proyectado', 'obra__total_aplicaciones']
        agrupar_por = {'obra' : models.Obra,
                       'pais' : models.Pais, 
                       'estado_obra' : models.EstadoObra,}
