from django.conf.urls.defaults import patterns, include, url
from django.conf import settings
from django.contrib import admin, databrowse

from obras.models import Proveedor, Categoria, Gasto, DetalleGasto, Concepto, Obra
from django.views.generic.simple import redirect_to
from ajax_select import urls as ajax_select_urls

admin.autodiscover()
databrowse.site.register(Proveedor)
databrowse.site.register(Categoria)
databrowse.site.register(Gasto)
databrowse.site.register(DetalleGasto)
databrowse.site.register(Concepto)
databrowse.site.register(Obra)

urlpatterns = patterns('',
    
    ('^$', redirect_to, {'url':'admin'}),    
    
    #SOLO PARA DESARROLLO    
    (r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
    url(r'^admin/', include(admin.site.urls)),
    (r'^databrowse/(.*)', databrowse.site.root),
    (r'^obras/', include('obras.urls')),
    
    # include the lookup urls
    (r'^admin/lookups/', include(ajax_select_urls)),
)
