BEGIN;
CREATE TABLE "obras_proveedor" (
    "id" serial NOT NULL PRIMARY KEY,
    "nombre" varchar(70) NOT NULL UNIQUE,
    "domicilio" varchar(50) NOT NULL,
    "localidad" varchar(50) NOT NULL,
    "cod_prov_id" integer NOT NULL,
    "cod_postal" varchar(30) NOT NULL,
    "telefono" varchar(50) NOT NULL,
    "fax" varchar(30),
    "cuit" bigint NOT NULL,
    "cod_impuesto" integer NOT NULL,
    "telefono_alternativo" varchar(50),
    "web" varchar(200),
    "email" varchar(75)
)
;
CREATE TABLE "obras_contactoproveedor" (
    "id" serial NOT NULL PRIMARY KEY,
    "proveedor_id" integer NOT NULL REFERENCES "obras_proveedor" ("id") DEFERRABLE INITIALLY DEFERRED,
    "nombre" varchar(30) NOT NULL,
    "email" varchar(75),
    "telefono" varchar(30),
    "celular" varchar(30),
    "nextel" varchar(30),
    "pin" varchar(30)
)
;
CREATE TABLE "obras_gasto" (
    "id" serial NOT NULL PRIMARY KEY,
    "fecha_op" date NOT NULL,
    "numero" integer NOT NULL,
    "fecha" date NOT NULL,
    "tipo_id" integer NOT NULL,
    "proveedor_id" integer NOT NULL REFERENCES "obras_proveedor" ("id") DEFERRABLE INITIALLY DEFERRED,
    "detalles" text NOT NULL,
    "observaciones" text,
    "moneda" integer NOT NULL,
    "imp_neto" numeric(10, 2) NOT NULL
)
;
CREATE TABLE "obras_detallegasto" (
    "id" serial NOT NULL PRIMARY KEY,
    "gasto_id" integer NOT NULL REFERENCES "obras_gasto" ("id") DEFERRABLE INITIALLY DEFERRED,
    "concepto_id" integer NOT NULL,
    "obra_id" integer,
    "detalle" varchar(50) NOT NULL,
    "imp_neto" numeric(10, 2) NOT NULL
)
;
CREATE TABLE "obras_cliente" (
    "id" serial NOT NULL PRIMARY KEY,
    "nombre" varchar(70) NOT NULL UNIQUE,
    "domicilio" varchar(50) NOT NULL,
    "localidad" varchar(50) NOT NULL,
    "cod_prov_id" integer NOT NULL,
    "cod_postal" varchar(30) NOT NULL,
    "telefono" varchar(50) NOT NULL,
    "fax" varchar(30),
    "cuit" bigint NOT NULL,
    "cod_impuesto" integer NOT NULL,
    "observaciones" text,
    "activo" boolean NOT NULL,
    "telefono_alternativo" varchar(50),
    "web" varchar(200),
    "email" varchar(75)
)
;
CREATE TABLE "obras_contactocliente" (
    "id" serial NOT NULL PRIMARY KEY,
    "proveedor_id" integer NOT NULL REFERENCES "obras_cliente" ("id") DEFERRABLE INITIALLY DEFERRED,
    "nombre" varchar(30) NOT NULL,
    "email" varchar(75),
    "telefono" varchar(30),
    "celular" varchar(30),
    "nextel" varchar(30),
    "pin" varchar(30)
)
;
CREATE TABLE "obras_obra" (
    "id" serial NOT NULL PRIMARY KEY,
    "nombre" varchar(70) NOT NULL UNIQUE,
    "fecha_creacion" date NOT NULL,
    "cliente_id" integer NOT NULL REFERENCES "obras_cliente" ("id") DEFERRABLE INITIALLY DEFERRED,
    "direccion" varchar(100),
    "estado_obra_id" integer NOT NULL,
    "observaciones" text,
    "activo" boolean NOT NULL
)
;
ALTER TABLE "obras_detallegasto" ADD CONSTRAINT "obra_id_refs_id_5c132de4" FOREIGN KEY ("obra_id") REFERENCES "obras_obra" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE TABLE "obras_presupuesto" (
    "id" serial NOT NULL PRIMARY KEY,
    "fecha_emision" date NOT NULL,
    "obra_id" integer NOT NULL REFERENCES "obras_obra" ("id") DEFERRABLE INITIALLY DEFERRED,
    "num_pres_ext" integer,
    "detalle_pres_ext" varchar(30),
    "proveedor_id" integer NOT NULL REFERENCES "obras_proveedor" ("id") DEFERRABLE INITIALLY DEFERRED,
    "descripcion" varchar(50) NOT NULL,
    "observaciones" text,
    "moneda" integer NOT NULL,
    "importe_contratado" numeric(10, 2) NOT NULL
)
;
CREATE TABLE "obras_presupuestotsya" (
    "id" serial NOT NULL PRIMARY KEY,
    "fecha_emision" date NOT NULL,
    "obra_id" integer NOT NULL UNIQUE REFERENCES "obras_obra" ("id") DEFERRABLE INITIALLY DEFERRED,
    "descripcion" varchar(50) NOT NULL,
    "observaciones" text,
    "moneda" integer NOT NULL,
    "importe_contratado" numeric(10, 2) NOT NULL,
    "importe_proyectado" numeric(10, 2) NOT NULL
)
;
CREATE TABLE "obras_honorario" (
    "id" serial NOT NULL PRIMARY KEY,
    "tipo_factura_id" integer NOT NULL,
    "n_factura" integer NOT NULL,
    "fecha" date NOT NULL,
    "descripcion" varchar(50) NOT NULL,
    "observaciones" text,
    "obra_id" integer NOT NULL REFERENCES "obras_obra" ("id") DEFERRABLE INITIALLY DEFERRED,
    "imp_neto" numeric(10, 2) NOT NULL,
    "imp_a_cobrar" numeric(10, 2) NOT NULL
)
;
CREATE TABLE "obras_pagoobra" (
    "id" serial NOT NULL PRIMARY KEY,
    "fecha_emision" date NOT NULL,
    "tipo_factura_id" integer NOT NULL,
    "n_factura" integer NOT NULL,
    "fecha" date NOT NULL,
    "descripcion" varchar(50) NOT NULL,
    "observaciones" text,
    "presupuesto_id" integer NOT NULL REFERENCES "obras_presupuesto" ("id") DEFERRABLE INITIALLY DEFERRED,
    "imp_neto" numeric(10, 2) NOT NULL
)
;
CREATE TABLE "obras_estadoobra" (
    "id" serial NOT NULL PRIMARY KEY,
    "descripcion" varchar(50) NOT NULL,
    "activo" boolean NOT NULL
)
;
ALTER TABLE "obras_obra" ADD CONSTRAINT "estado_obra_id_refs_id_6ff3d295" FOREIGN KEY ("estado_obra_id") REFERENCES "obras_estadoobra" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE TABLE "obras_pais" (
    "id" serial NOT NULL PRIMARY KEY,
    "descripcion" varchar(30) NOT NULL,
    "activo" boolean NOT NULL
)
;
CREATE TABLE "obras_provincia" (
    "id" serial NOT NULL PRIMARY KEY,
    "descripcion" varchar(30) NOT NULL,
    "pais_id" integer NOT NULL REFERENCES "obras_pais" ("id") DEFERRABLE INITIALLY DEFERRED,
    "activo" boolean NOT NULL
)
;
ALTER TABLE "obras_proveedor" ADD CONSTRAINT "cod_prov_id_refs_id_3ffdb530" FOREIGN KEY ("cod_prov_id") REFERENCES "obras_provincia" ("id") DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "obras_cliente" ADD CONSTRAINT "cod_prov_id_refs_id_6a77c3ae" FOREIGN KEY ("cod_prov_id") REFERENCES "obras_provincia" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE TABLE "obras_tipocomprobante" (
    "id" serial NOT NULL PRIMARY KEY,
    "codigo" varchar(30) NOT NULL,
    "descripcion" varchar(30) NOT NULL,
    "porcentaje" double precision NOT NULL,
    "activo" boolean NOT NULL
)
;
ALTER TABLE "obras_gasto" ADD CONSTRAINT "tipo_id_refs_id_2afeb715" FOREIGN KEY ("tipo_id") REFERENCES "obras_tipocomprobante" ("id") DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "obras_honorario" ADD CONSTRAINT "tipo_factura_id_refs_id_11600766" FOREIGN KEY ("tipo_factura_id") REFERENCES "obras_tipocomprobante" ("id") DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "obras_pagoobra" ADD CONSTRAINT "tipo_factura_id_refs_id_6b13a469" FOREIGN KEY ("tipo_factura_id") REFERENCES "obras_tipocomprobante" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE TABLE "obras_categoria" (
    "id" serial NOT NULL PRIMARY KEY,
    "codigo" varchar(4) NOT NULL UNIQUE,
    "descripcion" varchar(30) NOT NULL,
    "activo" boolean NOT NULL
)
;
CREATE TABLE "obras_concepto" (
    "id" serial NOT NULL PRIMARY KEY,
    "categoria_id" integer NOT NULL REFERENCES "obras_categoria" ("id") DEFERRABLE INITIALLY DEFERRED,
    "codigo" varchar(4) NOT NULL,
    "descripcion" varchar(30) NOT NULL,
    "activo" boolean NOT NULL,
    UNIQUE ("codigo", "categoria_id")
)
;
ALTER TABLE "obras_detallegasto" ADD CONSTRAINT "concepto_id_refs_id_2fafc5ad" FOREIGN KEY ("concepto_id") REFERENCES "obras_concepto" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX "obras_proveedor_cod_prov_id" ON "obras_proveedor" ("cod_prov_id");
CREATE INDEX "obras_contactoproveedor_proveedor_id" ON "obras_contactoproveedor" ("proveedor_id");
CREATE INDEX "obras_gasto_tipo_id" ON "obras_gasto" ("tipo_id");
CREATE INDEX "obras_gasto_proveedor_id" ON "obras_gasto" ("proveedor_id");
CREATE INDEX "obras_detallegasto_gasto_id" ON "obras_detallegasto" ("gasto_id");
CREATE INDEX "obras_detallegasto_concepto_id" ON "obras_detallegasto" ("concepto_id");
CREATE INDEX "obras_detallegasto_obra_id" ON "obras_detallegasto" ("obra_id");
CREATE INDEX "obras_cliente_cod_prov_id" ON "obras_cliente" ("cod_prov_id");
CREATE INDEX "obras_contactocliente_proveedor_id" ON "obras_contactocliente" ("proveedor_id");
CREATE INDEX "obras_obra_cliente_id" ON "obras_obra" ("cliente_id");
CREATE INDEX "obras_obra_estado_obra_id" ON "obras_obra" ("estado_obra_id");
CREATE INDEX "obras_presupuesto_obra_id" ON "obras_presupuesto" ("obra_id");
CREATE INDEX "obras_presupuesto_proveedor_id" ON "obras_presupuesto" ("proveedor_id");
CREATE INDEX "obras_honorario_tipo_factura_id" ON "obras_honorario" ("tipo_factura_id");
CREATE INDEX "obras_honorario_obra_id" ON "obras_honorario" ("obra_id");
CREATE INDEX "obras_pagoobra_tipo_factura_id" ON "obras_pagoobra" ("tipo_factura_id");
CREATE INDEX "obras_pagoobra_presupuesto_id" ON "obras_pagoobra" ("presupuesto_id");
CREATE INDEX "obras_provincia_pais_id" ON "obras_provincia" ("pais_id");
CREATE INDEX "obras_concepto_categoria_id" ON "obras_concepto" ("categoria_id");
COMMIT;
