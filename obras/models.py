# -*- encoding: utf-8 -*-
from django.db import models
from datetime import date

from decimal import Decimal
from django.db.models.aggregates import Sum

RI = 0
MT = 1

IMP_CHOICES = (
    (RI, 'Responsable Inscripto'),
    (MT, 'Monotributo'),
)

REM = 0
VIA = 1

GASTO_CHOICES = (
    (REM, 'Remís'),
    (VIA, 'Viáticos'),
)

PESOS = 1
URUGUAYOS = 2
REALES = 3
DOLARES = 4
EUROS = 5
BOLIVAR = 6


MONEDA_CHOICES = (
    (PESOS, 'Pesos Argentinos'),
    (URUGUAYOS, 'Pesos Uruguayos'),
    (REALES, 'Reales'),
    (DOLARES, 'Dólares'),
    (EUROS, 'Euros'),
    (BOLIVAR, 'Bolivar'),
)


class Proveedor(models.Model):
    nombre = models.CharField('Nombre', max_length=70, unique=True)
    domicilio = models.CharField(max_length=50, blank=True)
    localidad = models.CharField(max_length=50, blank=True)
    cod_prov = models.ForeignKey('Provincia', verbose_name='provincia', null=True, blank=True)
    cod_postal = models.CharField('Código postal', max_length=30, blank=True)
    telefono = models.CharField('Teléfono', max_length=50, blank=True)
    fax = models.CharField(max_length=30, blank=True, null=True)
    cuit = models.BigIntegerField(blank=True, null=True) #fixed length
    cod_impuesto = models.IntegerField('Código impuesto', max_length=3, choices=IMP_CHOICES, default=MT)
    
    telefono_alternativo = models.CharField('Teléfono alternativo', max_length=50, blank=True, null=True)
    web = models.URLField(blank=True, null=True)
    email = models.EmailField('E-Mail', blank=True) 
      
    
    class Meta:
        ordering = ["id"]
        verbose_name = "Proveedor"
        verbose_name_plural = "Proveedores"
    
    def __unicode__(self):
        return u'%d - %s' % (self.id, self.nombre)

class ContactoProveedor(models.Model):
    proveedor = models.ForeignKey(Proveedor)
    nombre = models.CharField('Nombre y apellido', max_length=30, blank=True, null=True)
    email = models.EmailField('E-Mail', blank=True, null=True)
    telefono = models.CharField('Teléfono', max_length=30, blank=True, null=True)
    celular = models.CharField('Celular', max_length=30, blank=True, null=True)
    nextel = models.CharField('Nextel', max_length=30, blank=True, null=True)
    pin = models.CharField('PIN', max_length=30, blank=True, null=True)
    
    def __unicode__(self):
        return u''
    
    class Meta:
        verbose_name = "Datos de contacto"
        verbose_name_plural = "Datos de contacto"
    

class Gasto(models.Model):
    fecha_op = models.DateField('Fecha del comprobante', default=date.today)
    numero = models.BigIntegerField(verbose_name='Código de comprobante externo')
    fecha = models.DateField('Fecha de alta sistema', default=date.today)
    tipo = models.ForeignKey('TipoComprobante', verbose_name='tipo de comprobante')
    proveedor = models.ForeignKey(Proveedor)
    detalles = models.TextField('Detalle breve', max_length=50)
    observaciones = models.TextField(blank=True, null=True)
    moneda = models.IntegerField(max_length=3, choices=MONEDA_CHOICES, default=PESOS)
    imp_neto = models.DecimalField('Importe neto no gravado', max_digits=10, decimal_places=2)
    
    def get_importe_a_pagar(self):
        return self.tipo.aplicar_porcentaje(self.imp_neto)
    
    get_importe_a_pagar.short_description = 'Importe a pagar'
    
    def esta_asignado(self):
        """ 
        Devuelve True si las aplicaciones cubren el importe total,
        y ninguna de ellas corresponde a un concepto no asignado 
        """
        
        total = 0
        for detalle in DetalleGasto.objects.filter(gasto=self):
            if detalle.concepto.no_asignado:
                return False
            total += detalle.imp_neto
        
        return self.imp_neto <= total
    
    def total_aplicaciones(self):
        """
        Devuelve la suma de los importes netos de los detalles
        gastos de este gasto.
        """
        
        tot_ap = sum([dg.imp_neto for dg in 
                      DetalleGasto.objects.filter(gasto=self)]) 
        
        return tot_ap if tot_ap else Decimal('0.00')
        
    def __unicode__(self):
        return '%s, Proveedor %s' % (self.detalles, self.proveedor,)
    
class DetalleGasto(models.Model):

    gasto = models.ForeignKey(Gasto)
    concepto = models.ForeignKey('Concepto')
    obra = models.ForeignKey('Obra', null=True, blank=False)
    detalle = models.CharField('Detalle breve', null=True, blank=True, max_length=50)
    imp_neto = models.DecimalField('Importe neto no gravado', max_digits=10, decimal_places=2)
    
    def get_importe_a_pagar(self):
        return self.gasto.tipo.aplicar_porcentaje(self.imp_neto)
    
    get_importe_a_pagar.short_description = 'Importe a pagar'

    def is_parent_obra(self, obra):
        """
        determina si un gasto pertenece a determinada obra
        @arg: obra Id de la obra a Checkar
        """
        if self.obra.id == obra:
            return True
        else:
            return False

    
    def __unicode__(self):
        return u''
    
    class Meta:
        verbose_name = "Imputación"
        verbose_name_plural = "Imputaciones"
        
class Cliente(models.Model):
    nombre = models.CharField('Nombre', max_length=70, unique=True)
    domicilio = models.CharField(max_length=50, blank=True)
    localidad = models.CharField(max_length=50, blank=True)
    cod_prov = models.ForeignKey('Provincia', verbose_name='provincia', null=True, blank=True)
    cod_postal = models.CharField('Código postal', max_length=30, blank=True)
    telefono = models.CharField('Teléfono', max_length=50, blank=True)
    fax = models.CharField(max_length=30, blank=True, null=True)
    cuit = models.BigIntegerField(blank=True, null=True) #fixed length
    cod_impuesto = models.IntegerField('Código Impuesto', max_length=3, choices=IMP_CHOICES, default=RI)
    observaciones = models.TextField(blank=True, null=True)
    activo = models.BooleanField()
    
    telefono_alternativo = models.CharField('Teléfono alternativo', max_length=50, blank=True, null=True)
    web = models.URLField(blank=True, null=True)
    email = models.EmailField('E-Mail', blank=True, null=True)
    
    def __unicode__(self):
        return u'%d - %s' % (self.id, self.nombre)
    
    class Meta:
        ordering = ('id',)
    
class ContactoCliente(models.Model):
    proveedor = models.ForeignKey(Cliente)#fixme llamar cliente
    nombre = models.CharField('Nombre y apellido', max_length=30, blank=True, null=True)
    email = models.EmailField('E-Mail', blank=True, null=True)
    telefono = models.CharField('Teléfono', max_length=30, blank=True, null=True)
    celular = models.CharField('Celular', max_length=30, blank=True, null=True)
    nextel = models.CharField('Nextel', max_length=30, blank=True, null=True)
    pin = models.CharField('PIN', max_length=30, blank=True, null=True)
    
    def __unicode__(self):
        return u''
    
    class Meta:
        verbose_name = "Datos de contacto"
        verbose_name_plural = "Datos de contacto"

class Obra(models.Model):
    nombre = models.CharField('Nombre', max_length=70, unique=True)
    fecha_creacion = models.DateField('Fecha de creación', default=date.today)
    cliente = models.ForeignKey(Cliente)
    pais = models.ForeignKey('Pais')
    direccion = models.CharField('Dirección', max_length=100, blank=True, null=True)
    estado_obra = models.ForeignKey('EstadoObra')
    observaciones = models.TextField(blank=True, null=True)
    activo = models.BooleanField()
    
    def total_aplicaciones(self):
        """
        Devuelve la suma de los honorarios de esta obra.
        """
        
        tot_ap = Honorario.objects.filter(
                                obra=self).aggregate(
                                Sum('imp_neto'))['imp_neto__sum']
        
        return tot_ap if tot_ap else Decimal('0.00')
    
    def total_presupuesto_asignado(self, proveedor):
        """ 
        Devuelve la suma de los presupuestos asignados a esta obra con el 
        proveedor especificado. 
        """
        
        tot_pres = Presupuesto.objects.filter(obra=self,
                        proveedor=proveedor).aggregate(
                    Sum('importe_contratado'))['importe_contratado__sum']
        
        return tot_pres if tot_pres else Decimal('0.00')

    def total_gastos_in_range(self, desde, hasta, proveedor=None):
        '''
        Devuelve el total de gastos de la obra en un rango 
        de fecha dados.
        '''
        tot_imp = DetalleGasto.objects.filter(obra=self,
                                          gasto__fecha_op__range=(desde,hasta))
        if proveedor:
            tot_imp = tot_imp.filter(gasto__proveedor=proveedor)
        
        tot_imp = tot_imp.aggregate(Sum('imp_neto'))['imp_neto__sum']

        return tot_imp if tot_imp else Decimal('0.00')

    def tiene_gastos_en_rango(self, desde, hasta, proveedor=None):
        '''
        Devuelve si esta obra tiene gastos en determinada fecha
        '''
        tiene = DetalleGasto.objects.filter(obra=self, gasto__fecha_op__range=(desde, hasta))
        if proveedor:
            tiene = tiene.filter(gasto__proveedor=proveedor)

        return True if tiene and tiene <> [] else False

    def get_categorias(self, desde, hasta, selected_cat=None):
        if selected_cat:
            return [selected_cat]
        else:
            gastos = DetalleGasto.objects.filter(obra=self,
                                             gasto__fecha_op__range=(desde,
                                                                     hasta))
            categorias = []
            for gasto in gastos:
                cat = gasto.concepto.categoria
                if not cat in categorias:
                    categorias.append(cat.id)
            return Categoria.objects.filter(pk__in=categorias).order_by('codigo')
    
    
    def __unicode__(self):
        return u'%d - %s' % (self.id, self.nombre)
    
    def save(self, **kwargs):
        if not self.activo:
            self.activo = self.estado_obra.activo
        super(Obra, self).save(**kwargs)
        
    class Meta:
        ordering = ('id',)

class Presupuesto(models.Model):
    fecha_emision = models.DateField('Fecha Emisión', default=date.today)
    obra = models.ForeignKey(Obra)
    num_pres_ext = models.CharField('Presupuesto Externo N°', max_length=30, blank=True, null=True) # opcional
    detalle_pres_ext = models.CharField('Detalle Presupuesto Externo', max_length=30, blank=True, null=True)
    proveedor = models.ForeignKey(Proveedor)
    descripcion = models.CharField(max_length=200)
    observaciones = models.TextField(blank=True, null=True)
    moneda = models.IntegerField(max_length=3, choices=MONEDA_CHOICES, default=PESOS)
    importe_contratado = models.DecimalField('Importe contratado', max_digits=10, decimal_places=2)
    
    #para compatibilidad con sistema viejo
    codigo_comprobante = models.IntegerField(blank=True, null=True)
    
    def total_aplicaciones(self):
        """
        Devuelve la suma de los pagos de este presupuesto.
        """
        
        tot_ap = PagoObra.objects.filter(
                            presupuesto=self).aggregate(
                            Sum('imp_neto'))['imp_neto__sum']
        
        return tot_ap if tot_ap else Decimal('0.00')
    
    class Meta:
        verbose_name = "presupuesto proveedores"
        verbose_name_plural = "presupuestos proveedores"
        ordering = ('id',)
    
    def __unicode__(self):
        return u'%d - %s' % (self.id, self.descripcion)
        

class PresupuestoTSYA(models.Model):
    fecha_emision = models.DateField('Fecha Emisión', default=date.today)
    obra = models.ForeignKey(Obra, unique=True)
    descripcion = models.CharField(max_length=50)
    observaciones = models.TextField(blank=True, null=True)
    moneda = models.IntegerField(max_length=3, choices=MONEDA_CHOICES, default=PESOS)
    importe_contratado = models.DecimalField('Importe contratado', max_digits=10, decimal_places=2)
    importe_proyectado = models.DecimalField('Importe proyectado', max_digits=10, decimal_places=2,
                                             blank=True, null=True)
    
    def __unicode__(self):
        return u'obra: %s' % self.obra.nombre
    
    class Meta:
        verbose_name = "Presupuesto TSYA"
        verbose_name_plural = "Presupuestos TSYA"


class Honorario(models.Model):
    tipo_factura = models.ForeignKey('TipoComprobante')
    n_factura = models.IntegerField('Factura Número')
    fecha = models.DateField('Fecha', default=date.today)
    descripcion = models.CharField(max_length=120)
    observaciones = models.TextField(blank=True, null=True) 
    obra = models.ForeignKey(Obra)
    imp_neto = models.DecimalField('Importe Neto Factura Actual', max_digits=10, decimal_places=2)
    imp_a_cobrar = models.DecimalField('Importe a cobrar', max_digits=10, decimal_places=2)
    
    def get_importe_a_cobrar(self):
        #return self.tipo_factura.aplicar_porcentaje(self.imp_neto)
        return self.imp_a_cobrar
        
    def __unicode__(self):
        return u'%s, Obra: %s' % (self.descripcion, self.obra.nombre)
    
    class Meta:
        unique_together = ("n_factura", "obra")

class PagoObra(models.Model):
    fecha_emision = models.DateField('Fecha Emisión', default=date.today)
    tipo_factura = models.ForeignKey('TipoComprobante')
    n_factura = models.IntegerField('Factura Número')
    fecha = models.DateField('Fecha', default=date.today)
    descripcion = models.CharField(max_length=130)
    observaciones = models.TextField(blank=True, null=True)
    presupuesto = models.ForeignKey(Presupuesto)
    imp_neto = models.DecimalField('Importe Neto Factura Actual', max_digits=10, decimal_places=2)
    imp_a_cobrar = models.DecimalField('Importe a cobrar', max_digits=10, decimal_places=2)
    
    def get_importe_a_cobrar(self):
        return self.imp_a_cobrar
    
    def __unicode__(self):
        return u'%d - %s' % (self.id, self.descripcion)
    
# ABM's basicos

class EstadoObra(models.Model):
    descripcion =  models.CharField(max_length=50, unique=True)
    activo = models.BooleanField(default=True)
    
    def __unicode__(self):
        return self.descripcion
        
class Pais(models.Model):
    descripcion = models.CharField('Descripción', max_length=30, unique=True)
    activo = models.BooleanField(default=True)

    class Meta:
        verbose_name = "Pais"
        verbose_name_plural = "Paises"

    def __unicode__(self):
        return u'%s' % (self.descripcion)

class Provincia(models.Model):
    descripcion = models.CharField('Descripción', max_length=30)
    pais = models.ForeignKey(Pais)  
    activo = models.BooleanField(default=True)

    def __unicode__(self):
        return u'%s' % (self.descripcion)
    
    class Meta:
        unique_together = ("pais", "descripcion")
        
class TipoComprobante(models.Model):
    codigo = models.CharField(max_length=30)
    descripcion = models.CharField('Descripción', max_length=30)
    porcentaje = models.FloatField(default=0)
    activo = models.BooleanField(default=True)
    
    
    def aplicar_porcentaje(self, valor):
        """
        toma el valor y lo incrementa en el porcentaje correspondiente a
        este tipo de comprobante.  
        """
        
        nuevo_valor = valor + valor * Decimal('%.2f' % (self.porcentaje / 100.0))
        return Decimal('%.2f' % (nuevo_valor))
    
    def __unicode__(self):
        return u'%s' % (self.descripcion)

class Categoria(models.Model):
    codigo = models.CharField('código', max_length=4, unique=True)
    descripcion = models.CharField('Descripción', max_length=30)
    activo = models.BooleanField(default=True)
    
    def get_superconceptos(self):
        """
        Devuelve una lista de conceptos pertenecientes a esta categoría, y
        que no son subconceptos, i.e., su codigo es de la forma x000
        """
        
        conceptos = Concepto.objects.order_by('codigo').filter(categoria=self)
        return [c for c in conceptos if c.is_superconcepto()]
        
    
    def __unicode__(self):
        return u'%s - %s' % (self.codigo, self.descripcion)
    
    class Meta:
        ordering = ('codigo',)

class Concepto(models.Model):
    categoria = models.ForeignKey(Categoria)
    codigo = models.CharField('código', max_length=4)
    descripcion = models.CharField('Descripción', max_length=30)
    no_asignado = models.BooleanField(default=False)
    
    activo = models.BooleanField(default=True)
    
    def codigo_completo(self):
        return '%s.%s' % (self.categoria.codigo, self.codigo)
    
    def is_superconcepto(self):
        return self.codigo[1:] == '000'
    
    def get_subconceptos(self):
        """ 
        Si este es un superconcepto, devuelve la lista de sus subconceptos, 
        i.e. los subconceptos de igual categoría e igual primer número de 
        código. 
        """ 
        
        conceptos = Concepto.objects.order_by('codigo').filter(
                                    categoria=self.categoria, 
                                    codigo__startswith=self.codigo[0])
        return [c for c in conceptos if c != self]
        
    
    class Meta:
        unique_together = ("codigo", "categoria")
        ordering = ('categoria__codigo', 'codigo')

    
    def __unicode__(self):
        return u'%s - %s' % (self.codigo_completo(), self.descripcion)
