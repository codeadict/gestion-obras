'''
Json views to return a filtered list of model names. Used to populate select
fields with autocomplete.
'''
from obras import models
from django.http import HttpResponse
from django.utils import simplejson

def get_conceptos(request):
    id_categoria = int(request.GET['id_categoria'])
    categoria = models.Categoria.objects.get(id=id_categoria)
    conceptos = models.Concepto.objects.filter(categoria=categoria)
    
    values = [c.id for c in conceptos]
    names = [str(c) for c in conceptos] 
        
    return HttpResponse(simplejson.dumps({'values':values, 'names':names}), 
                            mimetype='application/json')

def get_categoria(request):
    id_concepto = int(request.GET['id_concepto'])
    concepto = models.Concepto.objects.get(id=id_concepto)
        
    return HttpResponse(simplejson.dumps({'value': concepto.categoria.id}), 
                            mimetype='application/json')
    
def filter_presupuestos(request):
    id_proveedor = request.GET.get('id_proveedor')
    id_obra = request.GET.get('id_obra')
    
    presupuestos = models.Presupuesto.objects.all()
    
    if id_proveedor:
        id_proveedor = int(id_proveedor)
        presupuestos = presupuestos.filter(proveedor__id=id_proveedor)
        
    if id_obra:
        id_obra = int(id_obra)
        presupuestos = presupuestos.filter(obra__id=id_obra)
    
    values = [p.id for p in presupuestos]
    names = [str(p) for p in presupuestos] 
    
    return HttpResponse(simplejson.dumps({'values':values, 'names':names}), 
                            mimetype='application/json')
        

def filter_obras(request):
    id_proveedor = request.GET.get('id_proveedor')
    id_cliente = request.GET.get('id_cliente')
    
    obras = models.Obra.objects.all()
    
    if id_cliente:
        obras = obras.filter(cliente__id=int(id_cliente))
    
    if id_proveedor:
        id_proveedor = int(id_proveedor)
        
        #todos los presupuestos de los que participa este proveedor
        presupuestos = models.Presupuesto.objects.filter(
                                    proveedor__id=id_proveedor).order_by(
                                                                'obra__id')
        obras = []
        for presupuesto in presupuestos:
            if presupuesto.obra not in obras:
                obras.append(presupuesto.obra)
    
    
    values = [o.id for o in obras]
    names = [str(o) for o in obras] 
    
    return HttpResponse(simplejson.dumps({'values':values, 'names':names}), 
                            mimetype='application/json')

def filter_proveedor(request):
    
    id_obra = request.GET.get('id_obra')
    
    proveedores = models.Proveedor.objects.all()
    
    if id_obra:
        id_obra = int(id_obra)
        
        #todos los presupuestos de los que participa esta obra
        presupuestos = models.Presupuesto.objects.filter(
                                    obra__id=id_obra).order_by(
                                                            'proveedor__id')
        proveedores = []
        for presupuesto in presupuestos:
            if presupuesto.proveedor not in proveedores:
                proveedores.append(presupuesto.proveedor)      
    
    values = [o.id for o in proveedores]
    names = [str(o) for o in proveedores] 
    
    return HttpResponse(simplejson.dumps({'values':values, 'names':names}), 
                            mimetype='application/json')