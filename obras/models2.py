# -*- encoding: utf-8 -*-

from django.db import models

BA = 0
CBA = 1

PROV_CHOICES = (
    (BA, 'Buenos Aires'),
    (CBA, 'Córdoba'),
)

AR = 0
BRA = 1

PAIS_CHOICES = (
    (AR, 'Argentina'),
    (BRA, 'Brasil'),
)

RI = 0
MT = 1

IMP_CHOICES = (
    (RI, 'Responsable Inscripto'),
    (MT, 'Monotributo'),
)

REM = 0
VIA = 1

GASTO_CHOICES = (
    (REM, 'Remís'),
    (VIA, 'Viáticos'),
)

FA = 0
RE = 1

COMP_CHOICES = (
    (FA, 'Factura A'),
    (RE, 'Remito'),
)

PESOS = 0
DOL = 1
REALES = 2
EUROS = 3

MONEDA_CHOICES = (
    (PESOS, 'Pesos'),
    (DOL, 'Dólares'),
    (REALES, 'Reales'),
    (EUROS, 'Euros'),
)

class Proveedor(models.Model):
    nombre = models.CharField('Nombre', max_length=70)
    domicilio = models.CharField(max_length=50)
    localidad = models.CharField(max_length=20)
    cod_prov = models.IntegerField('Provincia', max_length=3, choices=PROV_CHOICES, default=BA)
    cod_postal = models.IntegerField('Código postal')
    telefono = models.IntegerField(help_text='Ingresar sólo los números sin puntos')    # En el admin pedir que se ingrese solo numero
    fax = models.IntegerField(help_text='Ingresar sólo los números sin puntos', blank=True)
    cuit = models.IntegerField() #fixed length
    cod_impuesto = models.IntegerField('Codigo provincia', max_length=3, choices=IMP_CHOICES, default=MT)  
    
    class Meta:
        ordering = ["nombre"]
        verbose_name = "Proveedor"
        verbose_name_plural = "Proveedores"
    
    def __unicode__(self):
        return u'%s' % (self.nombre)
    
    def get_full_name(self):
        pass

class OrdenPago(models.Model):
    #nro_op = models.AutoField(primary_key=True) #id
    fecha_op = models.DateField('Fecha OP')
    numero = models.IntegerField()
    fecha = models.DateField('Fecha')
    tipo = models.IntegerField(max_length=3, choices=GASTO_CHOICES, default=REM)
    obra = models.ForeignKey('Obra')
    proveedor = models.ForeignKey(Proveedor)
    detalles = models.CharField('Detalle breve', max_length=50)
    observaciones = models.CharField(max_length=300)
    imp_neto = models.DecimalField('Importe neto no gravado', max_digits=10, decimal_places=2)
    
    
    class Meta:
        verbose_name = "Orden de pago"
        verbose_name_plural = "Ordenes de pago"
    
    def __unicode__(self):
        return u'Tipo %s proveedor %s' % (self.get_tipo_display(), self.proveedor.nombre)
    

'''
class Proyecto(models.Model):
    obra =
    cliente = 
    cod_prov = models.IntegerField('Provincia', max_length=3, choices=PROV_CHOICES, default=BA)
    cod_pais = models.IntegerField('País', max_length=4, choices=PAIS_CHOICES, default=AR)
'''

class Cliente(models.Model):
    nombre = models.CharField('Nombre', max_length=70)
    domicilio = models.CharField(max_length=50)
    localidad = models.CharField(max_length=20)
    cod_prov = models.IntegerField('Provincia', max_length=3, choices=PROV_CHOICES, default=BA)
    cod_pais = models.IntegerField('País', max_length=3, choices=PAIS_CHOICES, default=AR)
    cod_postal = models.IntegerField('Código postal')
    telefono = models.IntegerField(help_text='Ingresar sólo los números sin puntos')    # En el admin pedir que se ingrese solo numero
    fax = models.IntegerField(help_text='Ingresar sólo los números sin puntos', blank=True)
    cuit = models.IntegerField() #fixed length
    cod_impuesto = models.IntegerField('Cód.Impuesto', max_length=3, choices=IMP_CHOICES, default=RI)
    observaciones = models.TextField(blank=True)
    activo = models.BooleanField()
    
    def __unicode__(self):
        return u'%s' % (self.nombre)

class Obra(models.Model):
    #ID - Codigo interno
    nombre = models.CharField('Nombre', max_length=70)
    fecha_creacion = models.DateField('Fecha de creación')
    cliente = models.ForeignKey(Cliente)
    estado_obra = models.ForeignKey('EstadoObra', unique=True)
    observaciones = models.TextField(blank=True)
    activo = models.BooleanField()
    
    def __unicode__(self):
        return u'Obra %s del cliente %s' % (self.nombre, self.cliente.nombre)
    
    def save(self, **kwargs):
        if not self.activo:
            self.activo = self.estado_obra.activo
        super(Obra, self).save(**kwargs)
        
class Presupuesto(models.Model):
    #ID - Codigo interno
    fecha_emision = models.DateField('Fecha Emisión') #mepa que es default=today
    obra = models.ForeignKey(Obra)
    num_pres_ext = models.IntegerField('Presupuesto Externo N°', max_length=30)
    proveedor = models.ForeignKey(Proveedor)
    tipo = models.IntegerField(max_length=3, choices=COMP_CHOICES, default=FA)
    descripcion = models.CharField(max_length=50)
    observaciones = models.TextField()
    moneda = models.IntegerField(max_length=3, choices=MONEDA_CHOICES, default=PESOS)
    importe_contratado = models.DecimalField('Importe contratado', max_digits=10, decimal_places=2)
    saldo = models.DecimalField('Saldo Contratado', max_digits=10, decimal_places=2)

    def __unicode__(self):
        return u'Proveedor: %s obra: %s moneda: %s  importe : %d' % (self.proveedor.nombre, self.obra.nombre, self.moneda, self.importe_contratado)

class Honorario(models.Model):
    #cliente =
    tipo_factura = models.IntegerField('Tipo', max_length=3, choices=COMP_CHOICES, default=FA)
    n_factura = models.IntegerField('Factura Número')
    fecha = models.DateField('Fecha')
    descripcion = models.CharField(max_length=50)
    observaciones = models.TextField()
    obra = models.ForeignKey(Obra)
    descripcion_ap = models.CharField(max_length=50, blank=True) 
    #estado_obra = models.CharField(max_length=50)
    importe_contratado = models.DecimalField('Importe neto no gravado', max_digits=10, decimal_places=2)
    total_aplicaciones = models.DecimalField('Total Aplicaciones', max_digits=10, decimal_places=2)
    imp_neto = models.DecimalField('Importe Neto Factura Actual', max_digits=10, decimal_places=2)
    imp_a_cobrar = models.DecimalField('Importe A Cobrar', max_digits=10, decimal_places=2)
    saldo = models.DecimalField('Importe neto no gravado', max_digits=10, decimal_places=2)
    imp_proyectado = models.DecimalField('Importe Proyectado No Contratado', max_digits=10, decimal_places=2)
    
    def __unicode__(self):
        pass
    
# ABM's basicos

class EstadoObra(models.Model):
    # ID - Codigo interno
    descripcion =  models.CharField(max_length=50)
    activo = models.BooleanField(default=True)
    
    def __unicode__(self):
        return u'%d - %s' % (self.id, self.descripcion)
        
class Pais(models.Model):
    # cod == id
    descripcion = models.CharField('Descripción', max_length=30)
    # pide cod y pais
    #¿que es?
    empresa = models.IntegerField()
    activo = models.BooleanField(default=True)

    def __unicode__(self):
        return u'%s' % (self.descripcion)

class Provincia(models.Model):
    # cod == id
    descripcion = models.CharField('Descripción', max_length=30)
    # pide cod y pais
    pais = models.ForeignKey(Pais)  
    activo = models.BooleanField(default=True)

    def __unicode__(self):
        return u'%s' % (self.descripcion)
        
class TipoComprobante(models.Model):
    codigo = models.IntegerField(max_length=3, choices=COMP_CHOICES, default=FA)
    descripcion = models.CharField('Descripción', max_length=30)
    activo = models.BooleanField(default=True)
    
    def __unicode__(self):
        return u'%s' % (self.descripcion)

class Conceptos(models.Model):
    # debe corresponder con tango
    codigo = models.IntegerField()
    descripcion = models.CharField('Descripción', max_length=30)
    activo = models.BooleanField(default=True)
    
    def __unicode__(self):
        return u'%s' % (self.descripcion)
