/*
 * Toma un id de un field correspondiente a una factura.
 * Lo limita a 12 digitos y le agrega padding de zeros. 
 */
function init_factura_field(id) {
	$('#' + id).css('width', '8em');
	$('#' + id).attr('size', 12);
	$('#' + id).attr('maxlength', 12);
	_pad_field(id);
	
	$('#' + id).change(function(){_pad_field(id);});
}

function _pad_field(id) {
	var val = $('#' + id).val()
	
	while (val.length < 12) {
		val = '0' + val;
	} 
	
	$('#' + id).val(val);
}