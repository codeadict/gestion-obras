# -*- encoding: utf-8 -*-
'''
Json views that send model info to ajax requests.
'''
from django.utils import simplejson
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_protect
from django.utils.encoding import smart_str, smart_unicode

from obras.models import Obra, Presupuesto, PresupuestoTSYA, Proveedor,\
    TipoComprobante
from django.core.exceptions import ObjectDoesNotExist
from decimal import Decimal

@csrf_protect
def get_obrahon_info(request):
    if request.is_ajax():
        id_obra = request.POST['obra_id']
        if id_obra:
            
            try:
                obra = Obra.objects.get(id=id_obra)
                pres = PresupuestoTSYA.objects.get(obra=obra)     
                
                aplicaciones = obra.total_aplicaciones()
                saldo = pres.importe_contratado - aplicaciones
            
                data = { 
                        'estado_obra': str(obra.estado_obra),
                        'desc_pres': str(pres.descripcion),
                        'imp_cont': str(pres.importe_contratado),
                        'tot_ap': str(aplicaciones),
                        'saldo': str(saldo),
                        'imp_proy': str(pres.importe_proyectado or ''),
                       }                
                
            except ObjectDoesNotExist:
                data = { 
                        'estado_obra': '',
                        'desc_pres': '',
                        'imp_cont': '',
                        'tot_ap': '',
                        'saldo': '',
                        'imp_proy': '',
                       }
            
            
            d = simplejson.dumps(data)
            return HttpResponse(d, mimetype = 'application/javascript')
    else:
        return HttpResponse(status=400)
        
def get_obra_info(request):
    if request.is_ajax():
        obra_id = request.POST['obra_id']
        if obra_id:
            obra = Obra.objects.get(pk=obra_id)
            data = {'nombre': obra.nombre, 'estado_obra': obra.estado_obra.descripcion}
            d = simplejson.dumps(data)
            return HttpResponse(d, mimetype = 'application/javascript')
    else:
        return HttpResponse(status=400)
        
def get_presupuesto_info(request):
    if request.is_ajax():
        id_obra = request.POST['obra_id']
        id_proveedor = request.POST['prov_id']
        id_presupuesto = request.POST['pres_id']
        if id_obra and id_proveedor:            
            try:
                obra = Obra.objects.get(id=id_obra)
                proveedor = Proveedor.objects.get(id=id_proveedor)
        
            except ObjectDoesNotExist:
                return HttpResponse("No existe la obra.")
            
            presupuestos = obra.presupuesto_set.filter(proveedor=proveedor)
            total_asignado = obra.total_presupuesto_asignado(proveedor)
            
            aplicaciones = 0
            if id_presupuesto:
                presupuesto = Presupuesto.objects.get(id=id_presupuesto)
                aplicaciones = presupuesto.total_aplicaciones()
            
            saldo = total_asignado - aplicaciones 
            
            data = { 'cant_pres': str(len(presupuestos)), 
                     'aplicaciones' : str(aplicaciones), 'saldo' : str(saldo),
                     'total_asignado' : str(total_asignado)}
            d = simplejson.dumps(data)
            return HttpResponse(d, mimetype = 'application/javascript')
    else:
        return HttpResponse(status=400)

def get_presupuestotsya_info(request):
    if request.is_ajax():
        id_obra = request.POST['obra_id']
        if id_obra:            
            try:
                obra = Obra.objects.get(id=id_obra)
            except ObjectDoesNotExist:
                return HttpResponse("No existe la obra.")
            
            data = {    'estado' :  obra.estado_obra.descripcion,
                        'id_cliente' : str(obra.cliente.id),
                        'cliente' : str(obra.cliente) }
            d = simplejson.dumps(data)
            return HttpResponse(d, mimetype = 'application/javascript')
    else:
        return HttpResponse(status=400)
        
def get_pago_obra_info(request):
    if request.is_ajax():
        pres_id = request.POST['pres_id']
        if pres_id:
            pres = Presupuesto.objects.get(pk=pres_id)
            obra = pres.obra
            
            total_aplicaciones = pres.total_aplicaciones()
            o_saldo = pres.importe_contratado - total_aplicaciones
            
            data = { 
                    'proveedor': smart_str(pres.proveedor),
                    'id_proveedor' : smart_str(pres.proveedor.id),
                    'obra': smart_str(pres.obra),
                    'id_obra' : smart_str(pres.obra.id),
                    'nro_pres_ext': smart_str(pres.num_pres_ext or ''),
                    'estado_obra': smart_str(obra.estado_obra),
                    'desc_pres': smart_str(pres.descripcion),
                    'obs_pres': smart_str(pres.observaciones),
                    'imp_cont': smart_str(pres.importe_contratado),
                    'tot_ap': smart_str(total_aplicaciones),
                    'saldo': smart_str(o_saldo),
                    }
            d = simplejson.dumps(data)
            return HttpResponse(d, mimetype = 'application/javascript')
    else:
        return HttpResponse(status=400)
    
def get_importe_a_cobrar(request):
    if request.is_ajax():
        importe = Decimal(request.POST['importe'])
        id_factura = int(request.POST['id_factura'])
        factura = TipoComprobante.objects.get(id=id_factura)
        
        importe_a_cobrar = factura.aplicar_porcentaje(importe)
        
        data = {'importe' : str(importe_a_cobrar)}
        
        d = simplejson.dumps(data)
        return HttpResponse(d, mimetype = 'application/javascript')
        