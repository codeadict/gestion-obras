#!/usr/bin/env python

import os

COMMAND_BLOB = 'pxview "{name}.DB" -r UTF-8 -b "{name}.MB" -c > "{name}.csv" 2> /dev/null'
COMMAND = 'pxview "{name}.DB" -r UTF-8 -c > "{name}.csv" 2> /dev/null'

def process(name):
    if (os.path.isfile('{name}.MB'.format(name=name))):
        print "converting with blob:", name
        os.system(COMMAND_BLOB.format(name=name))
    else:
        print "converting:", name
        os.system(COMMAND.format(name=name))

def post_process(name):
    fname = name +'.csv'
    fi = open(fname, 'rb')
    data = fi.read()
    fi.close()
    fo = open(fname, 'wb')
    fo.write(data.replace('\x00', ''))
    fo.close()


def clean_dir():
    os.system('rm *.csv')    

#MAIN PROGRAM
if __name__ == "__main__":
    
    clean_dir()    
    
    for fname in os.listdir(os.getcwd()):
        name, extension = fname.split('.')
        if extension == 'DB':
            process(name)
            post_process(name)
