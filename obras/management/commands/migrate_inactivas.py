# -*- encoding: utf-8 -*-
'''
Creates a valid default super user.
Runs with python manage.py superuser
'''
from django.core.management.base import BaseCommand
import settings
import csv
import datetime
from obras import models
from django.utils.datastructures import SortedDict
from django.db.utils import IntegrityError
from decimal import Decimal

FILE_PATH = settings.PROJECT_DIR + '/csv/'

DESCRIPCION_PAIS = {1 : 'Argentina',
                    2 : 'Brasil',
                    3 : 'Panamá',
                    4 : 'Uruguay',
                    5 : 'Paraguay',
                    6 : 'Chile',
                    7 : 'Centro America',
                    8 : 'Costa Rica',
                    9 : 'Republica Dominicana',
                    10 : 'Venezuela',
                    11 : 'Peru',
                    12 : 'Colombia',
                    13 : 'España',
                    }
DEFAULT_PAS = "País"
DESCRIPCION_PROVINCIA = {1 : 'Capital Federal',
                         2 : 'Buenos Aires',
                         3 : 'La pampa',
                         4 : 'Chaco',
                         7 : 'Jujuy',
                         12 : 'Tucuman',
                         13 : 'San Luis',
                         14 : 'San Juan',
                         16 : 'Santiago del Estero',
                         17 : 'Santa Fe',
                         18 : 'Cordoba',
                         20 : 'Corrientes',
                         22 : 'Salta',
                         23 : 'Catamarca',
                         24 : 'Misiones',
                         25 : 'Entre Rios',
                         26 : 'Neuquen',
                         28 : 'Chubut',
                         29 : 'Rio Negro',
                         30 : 'Santa Cruz',
                         31 : 'La Rioja',
                         32 : 'Mendoza',
                         33 : 'Formosa', }
DEFAULT_PROVINCIA = "Provincia"
ESTADOS = {1 : 'En gestión',
           4 : 'En curso',
           3 : 'Terminado',
           5 : 'Desactivado',
           6 : 'Archivado',
           }
CODIGO_ESTADO = {   541 : 1, 551 : 1, 5071 : 1, 5981 : 1,
                    542 : 4, 552 : 4, 5072 : 4, 5982 : 4,
                    543 : 3, 553 : 3, 5073 : 3, 5983 : 3,
                    544 : 5, 554 : 5, 5074 : 5, 5984 : 5,
                    545 : 6, 555 : 6, 5075 : 6, 5985 : 6, }
PAIS_ESTADO = {   541 : 1, 551 : 2, 5071 : 3, 5981 : 4,
                  542 : 1, 552 : 2, 5072 : 3, 5982 : 4,
                  543 : 1, 553 : 2, 5073 : 3, 5983 : 4,
                  544 : 1, 554 : 2, 5074 : 3, 5984 : 4,
                  545 : 1, 555 : 2, 5075 : 3, 5985 : 4, }
DEFAULT_ESTADO = "Estado obra"



def limpiar_observaciones(obs):
    try:
        obs = unicode(obs, errors='ignore')
        if '{' in obs:
            return ''
        return obs
    except UnicodeDecodeError:
        return ''
    

def limpiar_id(id_str):
    id_str = id_str.split('.')[0]
    return int(id_str)
    

def limpiar_telefono(telefono):
    #TODO inlcuir todos los telefonos cuando sean strings
    #return int(telefono.replace(' ', '').replace('-', '').split('/')[0])
    return telefono[:50]

def limpiar_fecha(fecha):
    if not fecha:
        return None
    
    try:
        return datetime.datetime.strptime(fecha, "%Y-%m-%d").date()
    except ValueError:
        return datetime.datetime.strptime(fecha, "%y-%m-%d").date()

def limpiar_importe(importe):
    return Decimal(importe or '0.00')

def limpiar_entero(numero):
    numero = numero.split('.')[0]
    
    return int(numero) if numero else 0

def limpiar_booleano(bool):
    return bool == '1'

def resolver_pais(cod_pais):
    
    return models.Pais.objects.get(id=int(cod_pais))

def pais_por_estado(cod_estado):
    return resolver_pais(PAIS_ESTADO[int(cod_estado)])

def resolver_provincia(cod_prov):
    if not cod_prov:
        cod_prov = '1'
    
    descripcion = DESCRIPCION_PROVINCIA.get(int(cod_prov), 'Provincia') 
    
    provincia = models.Provincia.objects.get_or_create(id=int(cod_prov),
                                    defaults={'descripcion' : descripcion,
                                              'pais' : resolver_pais("1")})[0]
    
    return provincia

def resolver_tipo_comprobante(cod_tipo):
    
    tipo, created = models.TipoComprobante.objects.get_or_create(
                                                        codigo=cod_tipo)
    
    if created:
        tipo.descripcion = cod_tipo
        tipo.save()
    
    return tipo

def resolver_categoria(codigo):
    
    tipo, created = models.Categoria.objects.get_or_create(
                                                        codigo=codigo)
    
    if created:
        tipo.descripcion = "CATEGORIA"
        tipo.save()
    
    return tipo

def resolver_estado(codigo):
    
    codigo_nuevo = CODIGO_ESTADO.get(int(codigo), 1)
    return models.EstadoObra.objects.get(id=codigo_nuevo)

""" 
Las funciones handler leen un archivo csv y vuelcan sus datos a modelos. 
"""
#TODO el patron de cada handler es identico, refactorizar en una clase

def handle_obra(doc):
    
    print "Migrando Obra"
    
    doc.next() #skip header
    
    for row in doc:
        obra = models.Obra()
        obra.id = limpiar_id(row[0])
        
        obra.activo = limpiar_booleano(row[5])
        obra.estado_obra = resolver_estado(row[4])
        
        #Solo obras SI desactivadas
        if obra.estado_obra.id == 5:
         
            obra.nombre = row[1]
            obra.fecha_creacion = limpiar_fecha(row[2])
            obra.cliente = models.Cliente.objects.get(id=int(row[3]))
                
            obra.pais = pais_por_estado(row[4])
            obra.observaciones = limpiar_observaciones(row[6])
                
            obra.save()
    
    print "Fin migracion obra."
        
    
def handle_presupuesto(doc):
    
    print "Migrando Presupuesto"
    
    doc.next() #skip header
    
    for row in doc:
        
        presupuesto = models.PresupuestoTSYA()
        presupuesto.id = limpiar_id(row[0])
        presupuesto.fecha_emision = limpiar_fecha(row[7])
        
        try:
            presupuesto.obra = models.Obra.objects.get(id=limpiar_id(row[1]))
            
            #No tocar presuspuestos de obras no inactivas
            if presupuesto.obra.estado_obra.id != 5:
                continue
            
        except models.Obra.DoesNotExist:
            print "No existe obra", limpiar_id(row[1])
            continue
        
        presupuesto.descripcion = limpiar_observaciones(row[5]) or 'HONORARIOS'
        presupuesto.observaciones = limpiar_observaciones(row[6])
        presupuesto.moneda = limpiar_entero(row[2])
        
        #TODO guardar importe proyectado en campo oculto
        presupuesto.importe_contratado = limpiar_importe(row[3])
        presupuesto.importe_proyectado = limpiar_importe(row[4])
        
        presupuesto.save()
        
    print "Fin migracion presupuesto."

        
def handle_comprobante_obra(doc):
    print "Migrando Comprobante obra"
    
    doc.next() #skip header
    
    for row in doc:
        presupuesto = models.Presupuesto()
        presupuesto.codigo_comprobante = limpiar_id(row[0])
        presupuesto.fecha_emision = limpiar_fecha(row[9])
        
        try:
            presupuesto.obra = models.Obra.objects.get(id=limpiar_id(row[1]))
            
            #No tocar presuspuestos de obras no inactivas
            if presupuesto.obra.estado_obra.id != 5:
                continue
            
        except models.Obra.DoesNotExist:
            print "No existe obra", limpiar_id(row[1])
            continue
        
        presupuesto.num_pres_ext = row[2]
        
        try:
            presupuesto.proveedor = models.Proveedor.objects.get(id=limpiar_id(row[3]))
        except:
            print "Provedor no existe", row[4]
            continue
        
        presupuesto.moneda = limpiar_entero(row[5])
        presupuesto.importe_contratado = limpiar_importe(row[6])
        presupuesto.descripcion = limpiar_observaciones(row[7]) or 'Comprobante'
        presupuesto.observaciones = limpiar_observaciones(row[8])
        
        presupuesto.save()
    
    print "Fin migracion comprobante obra."

def handle_factura(doc):
    print "Migrando Factura"
    
    doc.next() #skip header
    
    for row in doc:
        
        honorario = models.Honorario()
        honorario.id = limpiar_id(row[0])
        
        honorario.fecha = limpiar_fecha(row[1])
        honorario.tipo_factura = resolver_tipo_comprobante(row[2])
        honorario.n_factura = limpiar_entero(row[3])
        
        honorario.imp_a_cobrar = limpiar_importe(row[4])
        honorario.imp_neto = limpiar_importe(row[5])
        
        honorario.descripcion = limpiar_observaciones(row[6])
        honorario.observaciones = limpiar_observaciones(row[7]) 
        
        try:
            pres = models.PresupuestoTSYA.objects.get(id=limpiar_id(row[9]))
            honorario.obra = pres.obra
            
            #No tocar presuspuestos de obras no inactivas
            if pres.obra.estado_obra.id != 5:
                continue
            
        except models.PresupuestoTSYA.DoesNotExist:
            print "No existe presupuesto", limpiar_id(row[9])
            continue
        
        try:
            honorario.save()
        except IntegrityError:
            print "Honorario clave duplicada", row[3]
        
    
    print "Fin migracion factura."

def handle_factura_obra(doc):
        
    print "Migrando Factura obra"
    
    doc.next() #skip header
    
    for row in doc:
        pago = models.PagoObra()
        pago.id = limpiar_id(row[0])
        
        try:
            pago.presupuesto = models.Presupuesto.objects.get(
                                codigo_comprobante=limpiar_id(row[9]),
                                obra__id=limpiar_id(row[10]))
            
            #No tocar presuspuestos de obras no inactivas
            if pago.presupuesto.obra.estado_obra.id != 5:
                continue
            
        except models.Presupuesto.DoesNotExist:
            print "No existe presupuesto", limpiar_id(row[9])
            continue
        
        pago.fecha_emision = limpiar_fecha(row[1]) or pago.presupuesto.fecha_emision
        pago.fecha = limpiar_fecha(row[12])
        
        pago.tipo_factura = resolver_tipo_comprobante(row[2])
        pago.n_factura = limpiar_entero(row[3])
        
        pago.imp_a_cobrar = limpiar_importe(row[4])
        pago.imp_neto = limpiar_importe(row[5])
        
        pago.descripcion = limpiar_observaciones(row[6]) or 'Pago obra'
        pago.observaciones = limpiar_observaciones(row[7])
        
        #fulero
        if pago.n_factura > 2147483647:
            continue
        
        
        pago.save()
    
    print "Fin migracion factura obra."


def clean_concepto(concepto):
    if '.' not in concepto:
        concepto += '.'
    
    concepto += "0000"
    
    return concepto[:8].split('.')
    
    

def handle_detalle_orden(doc):
    print "Migrando Detalle orden de pago"
    
    doc.next() #skip header
    
    for row in doc:
        detalle = models.DetalleGasto()
        
        if row[1]:
            detalle.id = limpiar_id(row[1])
        
        try:
            detalle.gasto = models.Gasto.objects.get(id=limpiar_id(row[0]))    
        except:
            print "No existe Gasto", row[0], row[1]
            continue
        
        try:
            categoria, codigo = clean_concepto(row[2])
            
            detalle.concepto = models.Concepto.objects.get(codigo=codigo,
                                                           categoria__codigo=categoria)
        except models.Concepto.DoesNotExist:
            print "No existe Cocepto", limpiar_id(row[2])
            continue
        
        try:
            detalle.obra = models.Obra.objects.get(id=limpiar_id(row[3]))
            
            #No tocar detalles de obras no inactivas
            if detalle.obra.estado_obra.id != 5:
                continue
            
        except models.Obra.DoesNotExist:
            print "No existe Obra", limpiar_id(row[3])
            continue
        
        detalle.detalle = limpiar_observaciones(row[4])
        detalle.imp_neto = limpiar_importe(row[6])
        
        detalle.save()
    
    print "Fin migracion detalle orden de pago."

#FIXME, medio feo agregar uno por uno
HANDLERS = SortedDict()
HANDLERS['Obra.csv'] = handle_obra
HANDLERS['Presupuesto.csv'] = handle_presupuesto
HANDLERS['Comprobante Obra.csv'] = handle_comprobante_obra
HANDLERS['Factura Obra.csv'] = handle_factura_obra
HANDLERS['Factura.csv'] = handle_factura
HANDLERS['nueva.csv'] = handle_detalle_orden

class Command(BaseCommand):
    
    def handle(self, *args, **options):
        
        for filename in HANDLERS:
            f = open(FILE_PATH + filename, 'r')
            doc = csv.reader(f)
            HANDLERS[filename](doc)
        
        print "Fin migracion."

