from django.conf.urls.defaults import *
from obras.views import info_views, report_views, model_select_views
from obras import report_forms, models
from obras.generic_report.views import get_form, generic_report_view



orden_pago_data = {'report_template' : 'reports/ordenpago_report.html',
                   'pdf_filename' : 'Orden_de_pago_interna',
                   'model_class' : models.Gasto, 
                   'form_class' : report_forms.OrdenPagoReportForm, 
                   'field_name' : 'orden_pago'}

factura_data = {'report_template' : 'reports/factura_report.html',
                'pdf_filename' : 'Certificado_de_facturacion',
                'model_class' : models.Honorario, 
                'form_class' : report_forms.FacturaReportForm, 
                'field_name' : 'honorario'}

externa_data = {'report_template' : 'reports/externa_report.html',
                'pdf_filename' : 'Orden_de_pago',
                'model_class' : models.PagoObra, 
                'form_class' : report_forms.ExternaReportForm, 
                'field_name' : 'pago'}

presimp_data = {'report_template' : 'reports/presimp_report.html',
                'pdf_filename' : 'Presupuesto',
                'model_class' : models.Presupuesto, 
                'form_class' : report_forms.PresupuestoImpReportForm, 
                'field_name' : 'presupuesto'}

presupuestos_data = {'form_class' : report_forms.PresupuestosReportForm, 
                     'report_name' : 'Reporte Cuenta corriente obras',
                     'page_template' : 'reports/report_page.html',
                     'pdf_template' : 'reports/presobra_report.html',
                     'xls_template' : 'reports/presobra_report.html',
                     'report_filename' : 'Reporte_Cuenta_corriente_obra'}

ccanalitico_data = {'form_class' : report_forms.CuentaCorrienteReportForm, 
                    'report_name' : 'Reporte Honorarios cuenta corriente analitico',
                    'page_template' : 'reports/report_page.html',
                    'pdf_template' : 'reports/ccanalitico_report.html',
                    'xls_template' : 'reports/ccanalitico_report.html',
                    'report_filename' : 'Reporte_Honorarios_Cuenta_corriente_analitico'}

gastosobra_data = {'form_class' : report_forms.GastosObraReportForm, 
                    'report_name' : 'Reporte Gastos por proyecto',
                    'page_template' : 'reports/report_page.html',
                    'pdf_template' : 'reports/gastosobra_report.html',
                    'xls_template' : 'reports/gastosobra_report.html',
                    'report_filename' : 'Reporte_Gastos_por_obra'}

facturas_honorarios_data = {'form_class' : report_forms.FacturasHonorariosReportForm, 
                    'report_name' : 'Reporte Facturas honorarios TSYA',
                    'page_template' : 'reports/report_page.html',
                    'pdf_template' : 'reports/facturas_honorarios_report.html',
                    'xls_template' : 'reports/facturas_honorarios_report.html',
                    'report_filename' : 'Reporte_Facturas_Honorarios_TSYA'}

plan_cuentas_data = {'report_template' : 'reports/plancuenta_report.html', 
                     'pdf_filename' : 'Plan_de_cuentas'}

obras_estado_data = {'form_class' : report_forms.ObrasEstadoReportForm, 
                    'report_name' : 'Reporte Obras por estado',
                    'page_template' : 'reports/report_page.html',
                    'pdf_template' : 'reports/obrasestado_report.html',
                    'xls_template' : 'reports/obrasestado_report.html',
                    'report_filename' : 'Reporte_Obras_por_estado'}

reporte_generico_data = {'report_name' : 'Reporte Ad Hoc',
                         'page_template' : 'reports/generic_report_page.html',
                         'pdf_template' : 'reports/generic_report.html',
                         'xls_template' : 'reports/generic_report.html',
                         'report_filename' : 'reporte'}

urlpatterns = patterns('',
    
    url(r'^obra/$', info_views.get_obra_info, name='get_obra_info'),
    url(r'^importe/$', info_views.get_importe_a_cobrar, name='get_importe_a_cobrar'),
    url(r'^hon/$', info_views.get_obrahon_info, name='get_obrahon_info'),
    url(r'^presupuesto/$', info_views.get_presupuesto_info, name='get_presupuesto_info'),
    url(r'^presupuestotsya/$', info_views.get_presupuestotsya_info, name='get_presupuestotsya_info'),
    url(r'^pago_obra/$', info_views.get_pago_obra_info, name='get_pago_obra_info'),
    
    #reportes
    url(r'^ordenpago/(?P<object_id>\d+)/$', report_views.inline_report_view, orden_pago_data, name='orden_pago'),
    url(r'^factura/(?P<object_id>\d+)/$', report_views.inline_report_view, factura_data, name='factura'),
    url(r'^externa/(?P<object_id>\d+)/$', report_views.inline_report_view, externa_data, name='externa'),
    url(r'^presimp/(?P<object_id>\d+)/$', report_views.inline_report_view, presimp_data, name='presimp'),
    url(r'^presobra/$', report_views.report_view, presupuestos_data, name='pres_obra'),    
    url(r'^ccanalitico/$', report_views.report_view, ccanalitico_data, name='ccanalitico'),
    url(r'^gastosobra/$', report_views.report_view, gastosobra_data, name='gastosobra'),
    url(r'^facturas_honorarios/$', report_views.report_view, facturas_honorarios_data, name='facturas_honorarios'),
    url(r'^obrasestado/$', report_views.report_view, obras_estado_data, name='obrasestado'),    
    
    url(r'^reporte_generico/$', generic_report_view, reporte_generico_data, name='reporte_generico'),
    url(r'^get_form_generico/$', get_form, {'form_template' : 'reports/generic_form.html'}, 
        name='get_form_generico'),
    
    url(r'^cuentas/$', report_views.plan_cuentas, plan_cuentas_data, name='plan_cuentas'),
    
    #model select
    url(r'^get_conceptos/$', model_select_views.get_conceptos, name='get_conceptos'),
    url(r'^get_categoria/$', model_select_views.get_categoria, name='get_categoria'),
    url(r'^filter_presupuestos/$', model_select_views.filter_presupuestos, name='filter_presupuestos'),
    url(r'^filter_obras/$', model_select_views.filter_obras, name='filter_obras'),
    url(r'^filter_proveedor/$', model_select_views.filter_proveedor, name='filter_proveedor'),
)