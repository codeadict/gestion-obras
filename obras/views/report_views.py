'''
Views for generating reports.
'''

from django import http
from django.template import RequestContext
from django.shortcuts import render_to_response
from obras import report_forms
import datetime


def make_response(content, filename, extension='pdf', mimetype='application/pdf'):
    """
    Takes a pdf form output and returns the HttpResponse for the attachment,
    with the given filename and the current datetime.
    """
    
    response = http.HttpResponse(content, mimetype=mimetype)
                
    filename += datetime.datetime.now().strftime('-%d-%m-%y-%H-%M-%S')
    response['Content-Disposition'] = 'attachment; filename=%s.%s' % (
                                                        filename, extension)
                    
    return response
    

def report_view(request, form_class, report_name, page_template, 
                pdf_template, xls_template, report_filename):
    
    if request.method == 'POST':
        form = form_class(data=request.POST)
        if form.is_valid():
            
            if form.cleaned_data['formato'] == 'PDF':
                result = form.get_pdf(request, pdf_template)
            
                if result: #errores en pdf         
                    return make_response(result, report_filename)
            
            else:
                result = form.get_xls(request, xls_template)
                return make_response(result, report_filename, 'xls', 
                                     'text/xls')
    
    else:
        form = form_class()
    
    return render_to_response(page_template, {'title':report_name,
                                         'form':form },                                  
                                  context_instance = RequestContext(request),)


def plan_cuentas(request, report_template, pdf_filename):
    form = report_forms.PlanCuentasReportForm()
    result = form.get_pdf(request, report_template)
            
    return make_response(result, pdf_filename + '.pdf')


def inline_report_view(request, object_id, report_template, pdf_filename,
                       model_class, form_class, field_name):
    object = model_class.objects.get(id=object_id)
    form =  form_class()
    
    #fixme ugly
    form.cleaned_data = { field_name : object }
    result = form.get_pdf(request, report_template)
    
    return make_response(result, pdf_filename + '.pdf')
