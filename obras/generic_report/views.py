from django.http import HttpResponse
from django.utils import simplejson
from django.template.loader import get_template
from django.template.context import Context
from obras.generic_report import forms
from obras.views.report_views import report_view


#Ajax view for getting the form html
generic_forms = {'HON' : forms.HonorarioReportForm, 
                 'GAS' : forms.GastoReportForm, 
                 'DET' : forms.DetalleReportForm,
                 'PAG' : forms.PagoReportForm,
                 'PRE' : forms.PresupuestoReportForm,
                 'TSY' : forms.PresupuestoTSYAReportForm}

def get_form(request, form_template):
    elemento = request.GET['elemento']
    form = generic_forms[elemento]()
    
    template = get_template(form_template)
    html = template.render(Context({'form' : form}))
        
    return HttpResponse(simplejson.dumps({'form' : html}), 
                            mimetype='application/json')
    
    
def generic_report_view(request, *args, **kwargs):
    
    form_class = forms.ElementSelectForm
    
    if request.method == 'POST':
        form = forms.ElementSelectForm(request.POST)
        if form.is_valid():
            
            form_class = generic_forms[form.cleaned_data['elemento']]
    
    return report_view(request, form_class, *args, **kwargs)
    
    