'''
An entry of the generic report.
'''
from obras import models, utils
from decimal import Decimal
from django.utils.datastructures import SortedDict

def get_styled_tag(field, value, campos_monetarios, tag='td'):
    style = 'style="text-align:right;"' if field in campos_monetarios else ''
    return '<{tag} {style}>{value}</{tag}>'.format(tag=tag, value=value, 
                                                   style=style)

def r_getattr(instance, name):
    """ 
    Version recursiva de la funcion getattr, siguiendo la convencion de django
    de usar '__' para relaciones de foreign key.
    """
    for field_name in name.split('__'):
        instance = getattr(instance, field_name)
    
    return instance
    

#TODO crear base item
class ReportItem():
    """
    Representa una fila del reporte.
    """
    
    def __init__(self, instancia, columnas, campos_monetarios,
                 fecha_field):
        self.instancia = instancia
        self.columnas = columnas
        self.campos_monetarios = campos_monetarios
        self.fecha_field = fecha_field
        
    def __getitem__(self, name):
        
        instancia = r_getattr(self.instancia, name)
        
        if callable(instancia):
            #es una funcion
            return instancia()
        
        return instancia
    
    def _repr(self, name):
        """ Toma el valor y devuelve su representacion string """
        
        value = self[name]
        
        if name == self.fecha_field:
            return value.strftime('%d/%m/%Y')
        
        if name in self.campos_monetarios:
            return utils.money_format(value)
        
        return str(value)
        
    def as_table_row(self):
        """
        Devuelve una fila de una tabla html, con los valores de las columnas
        correspondientes a la instancia asignada al item.
        """
        tds = ''
        
        for col in self.columnas:
            value = self._repr(col)
            tds += get_styled_tag(col, value, self.campos_monetarios) 
        
        return tds

class EmptyItem():
    def __init__(self, size):
        self.size = size
        
    def as_table_row(self):
        return '<td></td>' * self.size

class TotalItem():
    """
    Item que va sumando subtotales a partir de otros items.
    """
    
    def __init__(self, columnas, campos_monetarios):
        self.columnas = columnas
        self.totales = SortedDict([(c, Decimal("0.0")) for c in campos_monetarios])
    
    def feed(self, item):
        """ Toma el item y suma sus valores al total calculado. """
        
        for campo in self.totales:
            self.totales[campo] += item[campo] or Decimal("0.0")
    
    def as_table_row(self):
        """
        Devuelve una fila de una tabla html, con los totales calculados
        en las columnas monetarias correspondientes.
        """
        
        tds=''
        for columna in self.columnas:
            if columna in self.totales:
                valor = '<b>' + utils.money_format(self.totales[columna]) + '</b>'
            else:
                valor = ''
                
            tds += get_styled_tag(columna, valor, self.totales.keys()) 
            
        return tds

class BaseReport():
    """
    Representa un reporte.
    """
    
    def __init__(self, fecha_desde, fecha_hasta, columnas,
                 agrupar_por=None, filtros={},
                 parciales=False, general=False):
        
        self.fecha_desde = fecha_desde
        self.fecha_hasta = fecha_hasta
        self.agrupar_por = agrupar_por
        self.filtros = filtros
        self.columnas = columnas
        self.parciales = parciales
        self.general = general
        
        self.titulo = self.Meta.titulo

        self.items = self._make_items()
    
    def _dividir_grupos(self, qs):
        """ 
        Toma un queryset y devuelve una lista de grupos de modelos.       
        """
        
        grupos = [[]]
        
        if qs:
            
            anterior = r_getattr(qs[0], self.agrupar_por)
            g_actual = 0
            for model in qs:
                valor_grupo = r_getattr(model, self.agrupar_por)
                if valor_grupo == anterior:
                    grupos[g_actual].append(model)
                else:
                    grupos.append([model])
                    g_actual += 1
                    anterior = valor_grupo
            
        return grupos
        
    
    def _get_queryset_groups(self):
        """ 
        Devuelve una lista de querysets, agrupados segun los parametros del
        reporte.
        """
        #Filtrar instancias elegidas
        args = self.filtros
        
        #Filtra fecha 
        args[self.Meta.fecha_field + '__range'] = (self.fecha_desde, 
                                                        self.fecha_hasta)
        qs = self.Meta.model.objects.filter(**args)
        
        if self.agrupar_por:
            #agrupa por el campo
            qs = qs.order_by(self.agrupar_por)
            return self._dividir_grupos(qs)
        
        return [qs] 
        
    def _make_items(self):
        
        grupos = self._get_queryset_groups()
        
        items = []
        total_general = TotalItem(self.columnas, self.Meta.campos_monetarios)
        for grupo in grupos:
            
            total_parcial = TotalItem(self.columnas, 
                                      self.Meta.campos_monetarios)
            
            for modelo in grupo:
                item = ReportItem(modelo, self.columnas, 
                                  self.Meta.campos_monetarios, 
                                  self.Meta.fecha_field)
                items.append(item)
                
                total_parcial.feed(item)
                total_general.feed(item)
            
            if self.parciales:
                items.append(total_parcial)
                items.append(EmptyItem(len(self.columnas)))
                items.append(EmptyItem(len(self.columnas)))
        
        if self.general:
            items.append(total_general)
            
        return items
    
    def as_table_header(self):
        """
        Devuelve una fila de una tabla html, con las columnas del reporte en 
        sus celdas.
        """
        tds = ''
        for col in self.columnas:
            valor = utils.field_pretty_print(col, self.Meta.model)
            tds += get_styled_tag(col, valor, self.Meta.campos_monetarios, 
                                  tag='th')
        return tds 
        
    class Meta:
        model = None
        fecha_field = 'fecha'
        campos_monetarios = ''
        titulo = 'Reporte'

class HonorarioReport(BaseReport):
    class Meta:
        model = models.Honorario
        fecha_field = 'fecha'
        campos_monetarios = ('imp_neto', 'imp_a_cobrar')
        titulo = "Reporte honorarios"

class GastoReport(BaseReport):
    class Meta:
        model = models.Gasto
        fecha_field = 'fecha_op'
        campos_monetarios = ('imp_neto', 'get_importe_a_pagar', 
                             'total_aplicaciones')
        titulo = "Reporte gastos"

class DetalleReport(BaseReport):
    class Meta:
        model = models.DetalleGasto
        fecha_field = 'gasto__fecha_op'
        campos_monetarios = ('imp_neto', 'get_importe_a_pagar')
        titulo = "Reporte detalle de gastos"

class PagoReport(BaseReport):
    class Meta:
        model = models.PagoObra
        fecha_field = 'fecha'
        campos_monetarios = ('imp_neto', 'imp_a_cobrar')
        titulo = "Reporte pagos de Obras"
        
class PresupuestoReport(BaseReport):
    class Meta:
        model = models.Presupuesto
        fecha_field = 'fecha_emision'
        campos_monetarios = ('importe_contratado', 'total_aplicaciones')
        titulo = "Reporte Presupuestos"
        
class PresupuestoTSYAReport(BaseReport):
    class Meta:
        model = models.PresupuestoTSYA
        fecha_field = 'fecha_emision'
        campos_monetarios = ('importe_contratado', 'importe_proyectado', 
                             'obra__total_aplicaciones')
        titulo = "Reporte Presupuestos TSyA"



    