# -*- encoding: utf-8 -*-
'''
Forms para generar reportes
'''

#Si no pincha pisa con muchos records
import sys
sys.setrecursionlimit(3000)

from django import forms
from django.template.loader import get_template
from django.template.context import Context, RequestContext
import cStringIO as StringIO
import ho.pisa as pisa
import os
from django.conf import settings
from obras import models
from datetime import date 
import utils
from decimal import Decimal
from django.contrib.admin import widgets

#FIXME en todos los reportes cambiar arrays por dicccionarios para mejorar la claridad
#FIXME las fechas del footer meterlas en los templates, no pasarlas

class BaseReportForm(forms.Form):
    
    FORMATO_CHOICES = (('PDF', 'Pdf'), ('XLS', 'Excel'))
    
    formato = forms.ChoiceField(choices=FORMATO_CHOICES)
    
    def _make_context_dict(self):
        """ 
        Returns a dictionary to pass to the report template as the context, 
        should be overriden by subclasses to fill the report data.
        """ 
        
        return {}
    
    def _render_template(self, request, template_name, format='pdf'):
        template = get_template(template_name)
        context = RequestContext(request, self._make_context_dict())
        context.update({'format':format, 'fecha': date.today(),
                        'pagesize': 'A4'})
        return template.render(context).encode("ISO-8859-1")
    
    def get_pdf(self, request, template_name):
        """ 
        Returns a pdf object that can be passed in an HttpResponse.
        """
        
        content = self._render_template(request, template_name)
        result = StringIO.StringIO()
        pdf = pisa.pisaDocument(StringIO.StringIO(content), dest=result,
                                 link_callback=fetch_resources)
        
        if pdf.err:
            errors = self._errors.setdefault(forms.forms.NON_FIELD_ERRORS, forms.util.ErrorList())
            errors.append("Error en el pdf: %s" % pdf.err)
            
            return None
        
        return result.getvalue()
    
    def get_xls(self, request, template_name):
        """ 
        Returns a xls string that can be passed in an HttpResponse.
        """
        
        return self._render_template(request, template_name, format='xls')
        

def fetch_resources(uri, rel):
    """ Needed by pisa to fetch static files such as images. """
    
    path = os.path.join(settings.MEDIA_ROOT, uri.replace(settings.MEDIA_URL, ""))
    return path


class OrdenPagoReportForm(BaseReportForm):
    orden_pago = forms.ModelChoiceField(label='Orden de pago',
                                        queryset=models.Gasto.objects.all())
    
    def _make_context_dict(self):
        orden_pago = self.cleaned_data['orden_pago']
        
        items = orden_pago.detallegasto_set.all()
        total = utils.comma(sum([dg.get_importe_a_pagar() for dg in items]))
    
        return { 'op': orden_pago, 'items': items, 'total': total}

class FacturaReportForm(BaseReportForm):
    honorario = forms.ModelChoiceField(label='Honorario',
                                        queryset=models.Honorario.objects.all())
    
    def _make_context_dict(self):
        honorario = self.cleaned_data['honorario']
        presupuesto = honorario.obra.presupuestotsya_set.all()[0]
        
        return { 'honorario': honorario, 'presupuesto' : presupuesto}

class ExternaReportForm(BaseReportForm):
    pago = forms.ModelChoiceField(label='Pago',
                                        queryset=models.PagoObra.objects.all())
    
    def _make_context_dict(self):
        pago = self.cleaned_data['pago']
        
        return { 'pago': pago }

class PresupuestoImpReportForm(BaseReportForm):
    presupuesto = forms.ModelChoiceField(label='Presupuesto',
                                        queryset=models.Presupuesto.objects.all())
    
    def _make_context_dict(self):
        presupuesto = self.cleaned_data['presupuesto']
        
        return { 'presupuesto': presupuesto }

class PresupuestosReportForm(BaseReportForm):
    obra = forms.ModelChoiceField(queryset=models.Obra.objects.all(), empty_label="Todas",
                                  help_text="Deje en blanco para incluir todas las obras.", required=False)
    desde = forms.DateField(widget=widgets.AdminDateWidget(), initial=utils.first_of_month)
    hasta = forms.DateField(widget=widgets.AdminDateWidget(), initial=date.today)
    proveedor = forms.ModelChoiceField(queryset=models.Proveedor.objects.all(), required=False)
    estado = forms.ModelChoiceField(queryset=models.EstadoObra.objects.all(), required=False)
    pais = forms.ModelChoiceField(queryset=models.Pais.objects.all(), required=False)
    cliente = forms.ModelChoiceField(queryset=models.Cliente.objects.all(), required=False)      
    
    def _get_obras(self):      
        pais = self.cleaned_data['pais']
        estado = self.cleaned_data['estado']
        cliente = self.cleaned_data['cliente']
        
        obras = models.Obra.objects.all()
        
        if estado:
            obras = obras.filter(estado_obra=estado)
        
        if pais:
            obras = obras.filter(pais=pais)
            
        if cliente: 
            obras = obras.filter(cliente=cliente)
            
        return obras
    
    def _create_item(self, obra, proveedor, desde, hasta):
        
        presupuestos = obra.presupuesto_set.order_by('id')
        
        if proveedor:
            presupuestos = presupuestos.filter(proveedor=proveedor)
        
        if len(presupuestos) == 0:
            return None
        
        principal = []

        total_presupuestado = Decimal("0.00")
        total_pagado = Decimal("0.00")
        saldo_total = Decimal("0.00")
        
        tiene_pagos = False
        
        for presupuesto in presupuestos:
            parcial = []
            
            saldo_parcial = presupuesto.importe_contratado
            saldo_total = saldo_total + saldo_parcial
            total_presupuestado = total_presupuestado + presupuesto.importe_contratado
            
            parcial.append((presupuesto, saldo_parcial, saldo_total))
            
            pagos = presupuesto.pagoobra_set.all().filter(fecha__range=(desde, hasta))
            
            if len(pagos) != 0:
                tiene_pagos = True
            
            for pago in pagos:
                saldo_parcial = saldo_parcial - pago.imp_neto
                saldo_total = saldo_total - pago.imp_neto
                total_pagado = total_pagado + pago.imp_neto
                
                parcial.append((pago, pago.imp_neto, saldo_total))

            parcial.append(('Saldo Parcial', saldo_parcial))
            principal.append(parcial)
            
        saldo = total_presupuestado - total_pagado
        
        if not tiene_pagos:
            return None

        return { 'obra': obra, 'p': presupuestos, 'principal': principal,
                                  'total_presupuestado' : total_presupuestado,
                                  'total_pagado' : total_pagado, 'saldo' : saldo}        
        
    
    def _make_context_dict(self):
        
        obra = self.cleaned_data['obra']
        desde = self.cleaned_data['desde']
        hasta = self.cleaned_data['hasta']
        proveedor = self.cleaned_data['proveedor']
        
        obras = [obra] if obra else self._get_obras()
        
        items = []
        for obra in obras:
            item = self._create_item(obra, proveedor, desde, hasta)
            
            if item:
                items.append(item)
            
        context = { 'items' : items }
        context.update(self.cleaned_data)
        
        return context
        

class CuentaCorrienteReportForm(BaseReportForm):
    
    obra = forms.ModelChoiceField(queryset=models.Obra.objects.all(), required=False)
    estado = forms.ModelChoiceField(queryset=models.EstadoObra.objects.all(), required=False)
    pais = forms.ModelChoiceField(queryset=models.Pais.objects.all(), required=False)
    cliente = forms.ModelChoiceField(queryset=models.Cliente.objects.all(), required=False)
    desde = forms.DateField(widget=widgets.AdminDateWidget(), initial=utils.first_of_month)
    hasta = forms.DateField(widget=widgets.AdminDateWidget(), initial=date.today)
    
    def clean_obra(self):
        obra = self.cleaned_data['obra']
        
        if not obra:
            return
        
        if not obra.presupuestotsya_set.all():
            raise forms.ValidationError("""No se ha agregado
                            el presupuesto correspondiente a TSYA.""")
    
        return obra
    
    def _make_context_dict(self):
        obra = self.cleaned_data['obra']
        desde = self.cleaned_data['desde']
        hasta = self.cleaned_data['hasta']
        estado = self.cleaned_data['estado']
        cliente = self.cleaned_data['cliente']
        pais = self.cleaned_data['pais']

        obras = models.Obra.objects.all()

        if obra:
            obras = [obra]

        if estado:
            obras = obras.filter(estado_obra=estado)
        if pais:
            obras = obras.filter(pais=pais)
        if cliente:
            obras = obras.filter(cliente=cliente)

        items = []
        
        for obra in obras:
            
            if not obra.presupuestotsya_set.all():
                break
            
            pres = obra.presupuestotsya_set.all()[0]
        
            principal = []
            principal.append((pres, pres.importe_contratado))
            total = pres.importe_contratado
            honorarios = obra.honorario_set.all().filter(fecha__range=(desde, hasta)).order_by('fecha')
            for honorario in honorarios:
                total -= honorario.imp_neto
                principal.append((honorario, '-' + utils.comma(honorario.imp_neto)))

            items.append({'obra': obra, 'pres' : pres, 'p': obra.presupuesto_set.all(),
                        'principal': principal, 'total': total})
                
        
        context = { 'items' : items}        
        context.update(self.cleaned_data)            
        return context

class PlanCuentasReportForm(BaseReportForm):
    
    def _make_context_dict(self):
        return {'conceptos' : models.Concepto.objects.all()}
    
class ObrasEstadoReportForm(BaseReportForm):
    estado = forms.ModelChoiceField(queryset=models.EstadoObra.objects.all(), required=False)
    pais = forms.ModelChoiceField(queryset=models.Pais.objects.all(), required=False)
    cliente = forms.ModelChoiceField(queryset=models.Cliente.objects.all(), required=False)
    desde = forms.DateField(widget=widgets.AdminDateWidget(), initial=utils.first_of_month)
    hasta = forms.DateField(widget=widgets.AdminDateWidget(), initial=date.today)
 
    def _get_obras(self, estado):
        desde = self.cleaned_data['desde']
        hasta = self.cleaned_data['hasta']
        cliente = self.cleaned_data['cliente']
        pais = self.cleaned_data['pais']
        
        obras = models.Obra.objects.filter(fecha_creacion__range=(desde, hasta))
        
        if estado:
            obras = obras.filter(estado_obra=estado)
            
        if pais:
            obras = obras.filter(pais=pais)
            
        if cliente: 
            obras = obras.filter(cliente=cliente)
            
        return obras        
    
    def _make_context_dict(self):
        estado = self.cleaned_data['estado']

        estados = [estado] if estado else models.EstadoObra.objects.all()

        items = []
        for estado in estados:
            items.append({ 'estado': estado, 'obras' : self._get_obras(estado) })

        context = { 'items' : items}
        context.update(self.cleaned_data)
        return context


class GastosObraReportForm(BaseReportForm):
    
    obra = forms.ModelChoiceField(queryset=models.Obra.objects.all(), required=False, empty_label="Todas",
                                  help_text="Deje en blanco para incluir todas las obras.",)
    estado = forms.ModelChoiceField(queryset=models.EstadoObra.objects.all(), required=False)
    pais = forms.ModelChoiceField(queryset=models.Pais.objects.all(), required=False)
    cliente = forms.ModelChoiceField(queryset=models.Cliente.objects.all(), required=False)
    proveedor = forms.ModelChoiceField(queryset=models.Proveedor.objects.all(), required=False)      
    desde = forms.DateField(widget=widgets.AdminDateWidget(), initial=utils.first_of_month)
    hasta = forms.DateField(widget=widgets.AdminDateWidget(), initial=date.today)
    abierto = forms.BooleanField(required=False, initial=True)
    categoria = forms.ModelChoiceField(label="Categoría", queryset=models.Categoria.objects.all(), required=False)
    concepto = forms.ModelChoiceField(label="Concepto", queryset=models.Concepto.objects.all(), required=False)
    
    def _make_context_dict(self):
        desde = self.cleaned_data['desde']
        hasta = self.cleaned_data['hasta']

        cliente = self.cleaned_data['cliente']
        obrad = self.cleaned_data['obra']
        estado = self.cleaned_data['estado']
        pais = self.cleaned_data['pais']
        proveedor = self.cleaned_data['proveedor']


        if obrad:
            obras = [obrad]
        else:
            obras = models.Obra.objects.distinct()

            if estado:
                obras = obras.filter(estado_obra=estado)
            if cliente:
                obras = obras.filter(cliente=cliente)
            if pais:
                obras = obras.filter(pais=pais)

        obras_1 =[]
        obras_data = []

        for obra in obras:
            have_gastos = obra.tiene_gastos_en_rango(desde, hasta)
            if proveedor:
                have_gastos = obra.tiene_gastos_en_rango(desde, hasta, proveedor)

            if have_gastos and obra not in obras_1:
                obras_1.append(obra)
                total_obra = obra.total_gastos_in_range(desde, hasta)
                if proveedor:
                    total_obra = obra.total_gastos_in_range(desde, hasta, proveedor)
                obras_data.append((obra, total_obra))

        #total de todas las obras sumadas
        total = sum([t[1] for t in obras_data])

        context = {'obras' : obras_data, 'total_general' : total }
        context.update(self.cleaned_data)        
        return context

class FacturasHonorariosReportForm(BaseReportForm):
    
    obra = forms.ModelChoiceField(queryset=models.Obra.objects.all(),
                                  required=False, empty_label="Todas",
                                  help_text="Deje en blanco para incluir todas las obras.")
    
    estado = forms.ModelChoiceField(queryset=models.EstadoObra.objects.all(), required=False)
    pais = forms.ModelChoiceField(queryset=models.Pais.objects.all(), required=False)
    cliente = forms.ModelChoiceField(queryset=models.Cliente.objects.all(), required=False)    
    
    desde = forms.DateField(widget=widgets.AdminDateWidget(), initial=utils.first_of_month)
    hasta = forms.DateField(widget=widgets.AdminDateWidget(), initial=date.today)
    
    def _get_honorarios(self, obra):
        desde = self.cleaned_data['desde']
        hasta = self.cleaned_data['hasta']
        pais = self.cleaned_data['pais']
        estado = self.cleaned_data['estado']
        cliente = self.cleaned_data['cliente']
        
        honorarios = models.Honorario.objects.filter(obra=obra,
                                fecha__range=(desde, hasta))
        
        if estado:
            honorarios = honorarios.filter(obra__estado_obra=estado)
        
        if pais:
            honorarios = honorarios.filter(obra__pais=pais)
            
        if cliente: 
            honorarios = honorarios.filter(obra__cliente=cliente)
            
        return honorarios

    
    def _make_context_dict(self):
        obra = self.cleaned_data['obra']
        obras = [obra] if obra else models.Obra.objects.all()
        
        items = []
        for obra in obras:
            honorarios = self._get_honorarios(obra)
            
            if honorarios:
                total_neto = sum([h.imp_neto for h in honorarios])
                total_a_cobrar = sum([h.imp_a_cobrar for h in honorarios])
                items.append((obra, honorarios, total_neto, total_a_cobrar))
        
        total_general_neto = sum(i[2] for i in items)
        total_general_a_cobrar = sum(i[3] for i in items)
        
        context = {'items' : items, 'total_neto' : total_general_neto,
                'total_a_cobrar' : total_general_a_cobrar}
        
        context.update(self.cleaned_data)
        return context
       
