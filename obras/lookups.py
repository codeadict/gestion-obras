# -*- encoding: utf-8 -*-
from ajax_select import LookupChannel
from obras.models import Obra
from django.db.models import Q
from django.utils.html import escape

class ObraLookup(LookupChannel):
    
    model = Obra
    
    def get_query(self,q,request):
        obras = Obra.objects.filter(Q(nombre__istartswith=q) | Q(id__istartswith=q)).order_by('nombre')
        
        if q.isdigit():
            return obras.order_by('id')
        else:
            return obras.order_by('nombre')
    # lo que muestra el autocomplete
#    def format_match(self,obj):
#        """ (HTML) formatted item for display in the dropdown """
#        #return self.format_item_display(obj)

    def format_item_display(self,obj):
        """ (HTML) formatted item for displaying item in the selected deck area """
        obra = obj
        if obra.estado_obra.descripcion != "En curso":
            mensaje = '<div style="color:#FF0000">Obra no esta en curso</div>'
        else: 
            mensaje = u''
            
        return LookupChannel.format_item_display(self, obj) + mensaje